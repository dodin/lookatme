package com.dodin.look.common

import com.dodin.look.data.dto.Comment
import org.hamcrest.Matchers
import org.junit.Test

import org.junit.Assert.*

class AppUtilsKtTest {

    @Test
    fun replaceTempComment() {
        val tempComment = Comment(0, 1,1, "New Comment")
        val comment1 = Comment(1, 1,1, "Comment 1")
        val comment2 = Comment(2, 1,1, "Comment 2")
        val comments = listOf(comment1, comment2, tempComment)
        val tempComments = mutableListOf(tempComment)
        val finalComments = listOf(comment1, comment2)

        val newComments = replaceTempComment(comments, tempComments)

        assertThat(newComments.count(), Matchers.`is`(2))
        assertEquals(newComments, finalComments)
        assertThat(tempComments, Matchers.empty())
    }
}