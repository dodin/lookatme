package com.dodin.look.ui.post

import com.dodin.look.common.replaceTempComment
import com.dodin.look.data.dto.Comment
import org.junit.Test

import org.junit.Assert.*
import org.hamcrest.Matchers.*

class PostViewModelTest {

    @Test
    fun replaceTempComment() {
        // Temp comment emited firstly from repository has id = 0
        val newCommentTemp =  Comment(
            postId = POST_ID,
            userId = USER_ID,
            content = "Hello my third new comment"
        )

        // After comment successfully sent to server it has some id
        val newComment = newCommentTemp.copy(
            id = 3
        )

        val tempComments = arrayListOf(
           newCommentTemp
        )

        val comments = listOf(
            Comment(
                id = 1,
                postId = POST_ID,
                userId = USER_ID,
                content = "First comment"
            ),
            Comment(
                id = 2,
                postId = POST_ID,
                userId = USER_ID,
                content = "Second comment"
            ),
            newCommentTemp,
            newComment
        )

        // Final comments after replaceTempComment
        val expectedComments = listOf(
            Comment(
                id = 1,
                postId = POST_ID,
                userId = USER_ID,
                content = "First comment"
            ),
            Comment(
                id = 2,
                postId = POST_ID,
                userId = USER_ID,
                content = "Second comment"
            ),
            newComment
        )

        val result = replaceTempComment(comments, tempComments)

        assertThat(result.size,  `is`(3))
        assertThat(result[2], samePropertyValuesAs(newComment))

        assertThat(tempComments, empty())
//        assertThat(result, contains(expectedComments))
        // how to compare 2 lists?
    }

    companion object {
        const val POST_ID = 2
        const val USER_ID = 1
    }
}