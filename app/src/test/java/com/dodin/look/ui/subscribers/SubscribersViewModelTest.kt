package com.dodin.look.ui.subscribers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import assertk.assertThat
import assertk.assertions.*
import com.dodin.look.App
import com.dodin.look.common.Lce
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.SubscribeResponse
import com.dodin.look.data.dto.User
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext.stopKoin

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class SubscribersViewModelTest {
    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    @After
    fun tearDown() {
        // Required because App will be recreated before each test
        stopKoin()
    }

    @Test
    fun `init viewmodel`() = runBlockingTest {
        val user = User(id = 1, username = "ivan01", name = "Ivan", subscribed = true)
        val user2 = User(id = 1, username = "john02", name = "John Doe")
        val user3 = User(id = 1, username = "john03", name = "John Boe")
        val subscribers = listOf(user)
        val subscriptions = listOf(user2, user3)

        // Given - user came from args
        val repo = mockk<Repository>()
        coEvery { repo.getSubscribers() }.returns(
            Lce.Success(
                subscribers
            )
        )
        coEvery { repo.getSubscriptions() }.returns(
            Lce.Success(
                subscriptions
            )
        )

        // When - viewmodel starts
        val viewModel = SubscribersViewModel(repo)

        // Then - check subscribers and subscriptions list
        assertThat(viewModel.subscribers.state().data!!).isEqualTo(subscribers)
        assertThat(viewModel.subscriptions.state().data!!).isEqualTo(subscriptions)
    }

    @Test
    fun `subscribe to subscriber`() = runBlockingTest {
        val user = User(id = 1, username = "john01", name = "John", subscribed = false)

        // Given - user came from args
        val repo = mockk<Repository>()
        coEvery { repo.getSubscribers() }.returns(
            Lce.Success(
                listOf(user)
            )
        )
        coEvery { repo.getSubscriptions() }.returns(
            Lce.Success(
                emptyList()
            )
        )
        // repo.subscribe will respond that current user subscribed to his subscriber
        coEvery { repo.subscribe(user.id) }.returns(
            Lce.Success(
                SubscribeResponse(2, user.id, true)
            )
        )

        val viewModel = SubscribersViewModel(repo)

        // When - user press unsubscribe
        viewModel.subscribe(user.id)

        // Then - subscription should be successfull
        val subscribers = viewModel.subscribers.state().data!!
        assertThat(subscribers.size).isEqualTo(1)
        assertThat(subscribers[0].subscribed).isTrue()
    }

    @Test
    fun `unsubscribe from subscription`() = runBlockingTest {
        val user = User(id = 1, username = "john01", name = "John", subscribed = true)

        // Given - user came from args
        val repo = mockk<Repository>()
        coEvery { repo.getSubscribers() }.returns(
            Lce.Success(
                emptyList()
            )
        )
        coEvery { repo.getSubscriptions() }.returns(
            Lce.Success(
                listOf(user)
            )
        )
        // repo.subscribe will respond that current user subscribed to his subscriber
        coEvery { repo.subscribe(user.id) }.returns(
            Lce.Success(
                SubscribeResponse(2, user.id, false)
            )
        )
        val viewModel = SubscribersViewModel(repo)

        // When - user press unsubscribe
        viewModel.subscribe(user.id)

        // Then - subscriptions should be empty
        val subscriptions = viewModel.subscriptions.state().data
        checkNotNull(subscriptions)
        assertThat(subscriptions).isEmpty()
    }
}