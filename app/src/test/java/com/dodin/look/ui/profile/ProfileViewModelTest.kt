package com.dodin.look.ui.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dodin.look.common.AppUtils
import com.dodin.look.common.Lce
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.SubscribeResponse
import com.dodin.look.data.dto.User
import com.dodin.look.data.local.Preferences
import com.dodin.look.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.Matchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class ProfileViewModelTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ProfileViewModel

    @After
    fun tearDown() {
        // Required because App will be recreated before each test
        StandAloneContext.stopKoin()
    }

    @Test
    fun `viewModel init with user`() = runBlockingTest {
        val user = User(id = 1, username = "john01", name = "John", email = "johndoe@mail.com")

        // Given - user came from args
        val pref = mockk<Preferences>()
        val appUtils = mockk<AppUtils>()
        val repo = mockk<Repository>()
        coEvery { repo.getUser(1) }.returns(flow { emit(Lce.Success(user)) })

        viewModel = ProfileViewModel(repo, pref, appUtils)


        // When - view model init with user
        viewModel.initUser(user)

        // Then - verify that observed user is from repo
        val loadedUser = viewModel.user.getOrAwaitValue()
        assertThat(loadedUser.data, `is`(user))
    }

    @Test
    fun `viewModel init with null`() = runBlockingTest {
        val user = User(id = 1, username = "john01", name = "John", email = "johndoe@mail.com")
        val user2 = User(id = 2, username = "john01", name = "John", email = "johndoe@mail.com")

        // Given - null came from args
        val pref = mockk<Preferences>()
        every { pref.user }.returns(user)
        val appUtils = mockk<AppUtils>()
        every { appUtils.getCurrentUserId() }.returns(user.id)
        val repo = mockk<Repository>()
        coEvery { repo.getUser(1) }.returns(flow { emit(Lce.Success(user2)) })

        viewModel = ProfileViewModel(repo, pref, appUtils)


        // When - view model init with null
        viewModel.initUser(null)

        // Then - verify that observed user is from repo and pref.user was called
        val loadedUser = viewModel.user.getOrAwaitValue()
        assertThat(loadedUser.data, `is`(user2))
        verify { pref.user }
    }

    @Test
    fun `subscribe to user`() = runBlockingTest {
        val user = User(id = 1, username = "john01", name = "John", email = "johndoe@mail.com")

        // Given - user came from args
        val pref = mockk<Preferences>()
        val appUtils = mockk<AppUtils>()
        val repo = mockk<Repository>()
        coEvery { repo.getUser(user.id) }.returns(flow { emit(Lce.Success(user)) })
        coEvery { repo.subscribe(user.id) }.returns(
            Lce.Success(SubscribeResponse(user.id, 2, true))
        )

        viewModel = ProfileViewModel(repo, pref, appUtils)
        viewModel.initUser(user)

        // When - subscribe
        viewModel.subscribe()

        // Then - user should be subscribed and subscribers must be incremented
        val result = viewModel.user.getOrAwaitValue().data
        checkNotNull(result)
        assertThat(result.subscribed, `is`(true))
        assertThat(result.subscribers, `is`(1))
    }
}
