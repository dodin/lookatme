package com.dodin.look.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dodin.look.common.typeconverters.DateTypeConverter
import com.dodin.look.common.typeconverters.StringListTypeConverter
import com.dodin.look.data.dto.Comment
import com.dodin.look.data.dto.User
import com.dodin.look.data.local.dto.Post

@Database(entities = [User::class, Post::class, Comment::class], version = 1)
@TypeConverters(DateTypeConverter::class, StringListTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao
}