package com.dodin.look.data.remote

import com.dodin.look.BuildConfig
import com.dodin.look.data.dto.*
import com.dodin.look.data.local.Preferences
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BackendService(private val gson: Gson, private val pref: Preferences) : Api {
    private val api: Api

    init {
        val httpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            httpClient.addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        // Auth interceptor adds token to headers
        httpClient.addInterceptor { chain ->
            val token = pref.token?.accessToken
            val originalRequest = chain.request()
            if (token?.isNotEmpty() == true) {
                val authRequest = originalRequest.newBuilder()
                    .header("Authorization", "Bearer $token")
                    .build()

                return@addInterceptor chain.proceed(authRequest)
            }

            return@addInterceptor chain.proceed(originalRequest)
        }

        // Content type interceptor
        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                .header("Accept", "application/json; utf-8")
                .header("Content-Type", "application/json")
                .method(original.method(), original.body())
                .build()

            return@addInterceptor chain.proceed(request);
        }


        val baseUrl = BuildConfig.HOST + "api/v1/"
        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))

        val retrofit = builder
            .client(httpClient.build())
            .build()

        api = retrofit.create<Api>(Api::class.java)
    }

    override suspend fun login(auth: AuthRequest): Token = api.login(auth)

    override suspend fun getPosts(sort: Int, page: Int): List<Post> = api.getPosts(sort, page)

    override suspend fun getPost(id: Int): Post = api.getPost(id)

    override suspend fun getUser(id: Int): User = api.getUser(id)

    override suspend fun getUserPosts(id: Int, page: Int) = api.getUserPosts(id, page)

    override suspend fun like(postId: Int): LikeResponse = api.like(postId)

    override suspend fun subscribe(id: Int): SubscribeResponse = api.subscribe(id)

    override suspend fun addComment(id: Int, comment: CommentRequest): Comment = api.addComment(id, comment)

    override suspend fun updatePhoto(image: MultipartBody.Part): User = api.updatePhoto(image)

    override suspend fun updateProfile(profile: ProfileRequest): User = api.updateProfile(profile)

    override suspend fun createPost(photos: Array<MultipartBody.Part>, post: Post): Post  = api.createPost(photos, post)

    override suspend fun getSubscribers(): List<User> = api.getSubscribers()

    override suspend fun getSubscriptions(): List<User> = api.getSubscriptions()

    override suspend fun search(query: String, page: Int): List<Post> = api.search(query, page)

    override suspend fun mentions(query: String): List<User> = api.mentions(query)

    override suspend fun updateFcmToken(token: String): String  = api.updateFcmToken(token)

    override suspend fun updatePost(postId: Int, photos: Array<MultipartBody.Part>, post: Post): Post = api.updatePost(postId, photos, post)
}