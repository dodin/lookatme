package com.dodin.look.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.dodin.look.common.Lce
import com.dodin.look.data.local.db.AppDatabase
import com.dodin.look.data.local.dto.Post
import com.dodin.look.data.mapper.PostMapper
import com.dodin.look.data.remote.Api
import com.dodin.look.data.remote.PostSort
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This boundary callback gets notified when user reaches to the edges of the list for example when
 * the database cannot provide any more data.
 **/
class PostsBoundaryCallback(
    private val sort: Int,
    private val api: Api,
    private val db: AppDatabase,
    private val mapper: PostMapper
) : PagedList.BoundaryCallback<Post>() {

    // Keep the last requested page. When the request is successful, increment the page number.
    private var lastRequestedPage = 1

    // Avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    val networkErrors: MutableLiveData<Lce<Unit>> = MutableLiveData()

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        requestAndSaveData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Post) {
        super.onItemAtEndLoaded(itemAtEnd)
        requestAndSaveData()
    }

    private fun requestAndSaveData() = GlobalScope.launch {
        if (isRequestInProgress) return@launch

        try {
            isRequestInProgress = true

            val response = api.getPosts(sort, lastRequestedPage)
            db.postDao().insert(mapper.toDb(response, sort == PostSort.FEED.type))
            db.commentDao().insertAll(response.flatMap { it.comments })
            db.userDao().insertAll(response.map { it.author })
            lastRequestedPage++
            networkErrors.postValue(Lce.Success(Unit))

            isRequestInProgress = false
        } catch (e: Exception) {
            networkErrors.postValue(Lce.Error(e.localizedMessage))
            isRequestInProgress = false
        }
    }
}