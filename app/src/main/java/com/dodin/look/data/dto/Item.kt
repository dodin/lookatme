package com.dodin.look.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item(
    val title: String = "",
    val link: String = "",
    val size: String = "",
    val color: String = ""
) : Parcelable