package com.dodin.look.data.remote

import com.dodin.look.data.dto.AuthRequest
import com.dodin.look.data.dto.Token
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.POST

interface Oauth {
    @POST("token")
    fun login(@Body auth: AuthRequest) : Deferred<Token>
    @POST("token/refresh")
    fun refresh(refreshToken: String) : Deferred<Token>
}