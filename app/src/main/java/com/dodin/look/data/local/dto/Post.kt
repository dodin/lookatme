package com.dodin.look.data.local.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.dodin.look.common.typeconverters.DateTypeConverter
import com.dodin.look.common.typeconverters.StringListTypeConverter
import java.util.*

@Entity
data class Post(
    @PrimaryKey
    var id: Int = 0,

    var title: String = "",
    var description: String = "",

    var like: Boolean = false,

    @ColumnInfo(name = "user_id")
    var userId: Int = 0,

    @TypeConverters(StringListTypeConverter::class)
    var photos: List<String> = emptyList(),

    @TypeConverters(DateTypeConverter::class)
    @ColumnInfo(name = "created_at")
    var createdAt: Date = Date(),

    var likes: Int = 0,

    @ColumnInfo(name = "is_feed")
    var isFeed: Boolean = false
)