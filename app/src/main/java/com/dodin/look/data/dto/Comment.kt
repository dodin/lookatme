package com.dodin.look.data.dto

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity
data class Comment(
    @PrimaryKey
    var id: Int = 0,

    @SerializedName("post_id")
    @ColumnInfo(name = "post_id")
    var postId: Int = 0,

    @SerializedName("user_id")
    @ColumnInfo(name = "user_id")
    var userId: Int = 0,

    var content: String = "",

    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: Date = Date(),

    @Ignore
    val author: User = User()
) : Parcelable