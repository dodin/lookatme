package com.dodin.look.data.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    var id: Int = 0,
    val title: String = "",
    val description: String = "",
    val like: Boolean = false,
    val author: User = User(),
    val photos: List<String> = emptyList(),
    val items: List<Item> = emptyList(),
    val comments: List<Comment> = emptyList(),
    @SerializedName("created_at")
    val createdAt: Long = 0,

    @SerializedName("user_id")
    val userId: Int = 0,

    val likes: Int = 0
) : Parcelable