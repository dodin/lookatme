package com.dodin.look.data.dto

import com.google.gson.annotations.SerializedName

data class LikeResponse(
    @SerializedName("post_id")
    val postId: Int = 0,
    @SerializedName("user_id")
    val userId: Int = 0,
    val like: Boolean = false
)