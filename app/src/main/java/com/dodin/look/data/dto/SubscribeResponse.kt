package com.dodin.look.data.dto

import com.google.gson.annotations.SerializedName

data class SubscribeResponse(
    @SerializedName("user_id")
    val userId: Int = 0,
    @SerializedName("subscriber_id")
    val subscriberId: Int = 0,
    val subscribed: Boolean = false
)