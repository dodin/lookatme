package com.dodin.look.data.dto

import com.google.gson.annotations.SerializedName

data class Token(
    val expires: String = "",
    @SerializedName("access_token")
    val accessToken: String = "",
    @SerializedName("refresh_token")
    val refreshToken: String = ""
)