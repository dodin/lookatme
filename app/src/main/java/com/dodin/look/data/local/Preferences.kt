package com.dodin.look.data.local

import android.content.Context
import android.preference.PreferenceManager
import androidx.core.content.edit
import com.dodin.look.data.dto.Token
import com.dodin.look.data.dto.User
import com.google.gson.Gson

class Preferences(c: Context, private val gson: Gson) {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(c)

    var user: User?
        get() = gson.fromJson(prefs.getString(FIELD_USER, null), User::class.java)
        set(value) {
            prefs.edit {
                putString(FIELD_USER, gson.toJson(value))
            }
        }

    var token: Token?
        get() = gson.fromJson(prefs.getString(FIELD_TOKEN, null), Token::class.java)
        set(value) {
            prefs.edit {
                putString(FIELD_TOKEN, gson.toJson(value))
            }
        }

    var fcmToken: String
        get() = prefs.getString(FIELD_FCM_TOKEN, "")!!
        set(value) {
            prefs.edit {
                putString(FIELD_FCM_TOKEN, value)
            }
        }

    companion object {
        const val FIELD_USER = "user"
        const val FIELD_TOKEN = "token"
        const val FIELD_FCM_TOKEN = "fcm_token"
    }
}