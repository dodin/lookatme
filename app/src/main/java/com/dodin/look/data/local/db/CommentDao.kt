package com.dodin.look.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dodin.look.data.dto.Comment

@Dao
interface CommentDao {
    @Query("SELECT * FROM comment WHERE post_id = :postId")
    suspend fun findByLookId(postId: Int): List<Comment>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(comment: Comment)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(comments: List<Comment>)
}