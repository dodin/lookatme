package com.dodin.look.data.mapper

import com.dodin.look.data.dto.Comment
import com.dodin.look.data.local.db.AppDatabase
import com.dodin.look.data.local.dto.Post as DbPost
import com.dodin.look.data.dto.Post
import com.dodin.look.data.dto.User
import java.util.*

/**
 * Helper class that allows to split Post data to separate classes.
 * Or combine Post object from different tables of db
 */
class PostMapper(val db: AppDatabase) {
    /**
     * Map list of local posts into full Post dto
     */
    suspend fun fromDb(post: List<DbPost>): List<Post> {
        return post.map {
            fromDb(it)
        }
    }

    /**
     * Map local post into full post
     */
    suspend fun fromDb(post: DbPost): Post {
        val comments = db.commentDao().findByLookId(post.id).map { commentsFromDb(it) }
        val user = db.userDao().getUser(post.userId)

        return Post(
            post.id,
            post.title,
            post.description,
            post.like,
            user ?: User(),
            post.photos,
            listOf(), // todo: save items
            comments,
            post.createdAt.time,
            post.userId,
            post.likes
        )
    }

    /**
     * Map list of full post objects to db post object
     */
    suspend fun toDb(post: List<Post>, isFeed: Boolean = false): List<DbPost> {
        return post.map {
            toDb(it, isFeed)
        }
    }

    /**
     * Map full post object to db post object
     */
    suspend fun toDb(post: Post, isFeed: Boolean = false): DbPost {
        return DbPost(
            post.id,
            post.title,
            post.description,
            post.like,
            post.userId,
            post.photos,
            Date(post.createdAt),
            post.likes,
            isFeed
        )
    }

    /**
     * Extract comments from post and prepare them to save into db
     */
    fun commentsToDb(post: Post): List<Comment> {
        return post.comments.map {
            Comment(
                it.id,
                post.id,
                it.author.id,
                it.content,
                it.createdAt
            )
        }
    }

    /**
     * Extract users from from post comments
     */
    fun commentsUsersToDb(post: Post): List<User> {
        return post.comments.map {
            it.author
        }
    }

    /**
     * Get comments from db and attach user to them
     */
    suspend fun commentsFromDb(comment: Comment): Comment {
        val user = db.userDao().getUser(comment.userId) ?: User()
        return comment.copy(author = user)
    }
}