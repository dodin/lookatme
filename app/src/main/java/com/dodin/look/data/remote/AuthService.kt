package com.dodin.look.data.remote

import com.dodin.look.BuildConfig
import com.dodin.look.data.dto.AuthRequest
import com.dodin.look.data.dto.Token
import com.dodin.look.data.local.Preferences
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthService(private val gson: Gson, private val pref: Preferences) : Oauth {
    private val api: Oauth

    init {
        val httpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        // Content type interceptor
        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                .header("Accept", "application/json")
                .method(original.method(), original.body())
                .build()

            return@addInterceptor chain.proceed(request);
        }


        val baseUrl = BuildConfig.HOST + "oauth/"
        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())

        val retrofit = builder
            .client(httpClient.build())
            .build()

        api = retrofit.create(Oauth::class.java)
    }

    override fun login(auth: AuthRequest): Deferred<Token> = api.login(auth)

    override fun refresh(refreshToken: String): Deferred<Token> = api.refresh(refreshToken)
}