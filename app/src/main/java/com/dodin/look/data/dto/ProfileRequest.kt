package com.dodin.look.data.dto

data class ProfileRequest(
    val name: String = "",
    val username: String = "",
    val about: String = "",
    val website: String = ""
)