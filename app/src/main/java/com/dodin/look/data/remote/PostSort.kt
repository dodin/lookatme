package com.dodin.look.data.remote

enum class PostSort(val type: Int) {
    NEW(1), POPULAR(2), FEED(3), FAVORITES(4)
}