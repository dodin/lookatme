package com.dodin.look.data.remote

import com.dodin.look.data.dto.*
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import retrofit2.http.*

interface Api {
    @POST("login")
    suspend fun login(@Body auth: AuthRequest): Token

    @GET("posts")
    suspend fun getPosts(@Query("sort") sort: Int = 0, @Query("page") page: Int = 0): List<Post>

    @GET("search")
    suspend fun search(@Query("q") query: String, @Query("page") page: Int): List<Post>

    @GET("mentions")
    suspend fun mentions(@Query("q") query: String): List<User>

    @GET("posts/{id}")
    suspend fun getPost(@Path("id") id: Int): Post

    @GET("users/{id}/posts")
    suspend fun getUserPosts(@Path("id") id: Int, @Query("page") page: Int = 0): List<Post>

    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: Int): User

    @POST("posts/{id}/like")
    suspend fun like(@Path("id") postId: Int): LikeResponse

    @POST("users/{id}/subscribe")
    suspend fun subscribe(@Path("id") id: Int): SubscribeResponse

    @POST("posts/{id}/comments")
    suspend fun addComment(@Path("id") id: Int, @Body comment: CommentRequest): Comment

    @POST("users/photo")
    @Multipart
    suspend fun updatePhoto(@Part image: MultipartBody.Part): User

    @PATCH("users")
    suspend fun updateProfile(@Body profile: ProfileRequest): User

    @POST("posts")
    @Multipart
    suspend fun createPost(@Part photos: Array<MultipartBody.Part>, @Part("post") post: Post): Post

    @POST("posts/{id}")
    @Multipart
    suspend fun updatePost(@Path("id") postId: Int, @Part photos: Array<MultipartBody.Part>, @Part("post") post: Post): Post

    @GET("user/subscribers")
    suspend fun getSubscribers(): List<User>

    @GET("user/subscriptions")
    suspend fun getSubscriptions(): List<User>

    @POST("user/fcm")
    suspend fun updateFcmToken(@Query("token") token: String): String
}