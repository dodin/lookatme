package com.dodin.look.data.local.db

import androidx.paging.DataSource
import androidx.room.*
import com.dodin.look.data.local.dto.Post

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(post: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(posts: List<Post>)

    @Update
    suspend fun update(post: Post)

    @Query("SELECT * FROM post")
    suspend fun getPosts(): List<Post>

    @Query("SELECT * FROM post WHERE id = :id")
    suspend fun findById(id: Int): Post

    @Query("SELECT * FROM post ORDER BY created_at DESC")
    fun getNew(): DataSource.Factory<Int, Post>

    @Query("SELECT * FROM post ORDER BY likes DESC")
    fun getPopular(): DataSource.Factory<Int, Post>

    @Query("SELECT * FROM post WHERE is_feed = 1 ORDER BY created_at DESC")
    fun getFeed(): DataSource.Factory<Int, Post>

    @Query("SELECT * FROM post WHERE `like` = 1")
    fun getFavorites(): DataSource.Factory<Int, Post>
}