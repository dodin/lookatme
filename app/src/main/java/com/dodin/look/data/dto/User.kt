package com.dodin.look.data.dto

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class User (
    @PrimaryKey
    var id: Int = 0,
    val token: String = "",
    val username: String = "",
    val name: String = "",
    val email: String = "",
    val photo: String = "",
    val about: String = "",
    val website: String = "",
    val posts: Int = 0,
    val subscribers: Int = 0,
    val subscriptions: Int = 0,
    val subscribed: Boolean = false
): Parcelable