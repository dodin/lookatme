package com.dodin.look.data.dto

data class CommentRequest(
    val content: String = ""
)