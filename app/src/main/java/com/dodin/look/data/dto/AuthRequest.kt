package com.dodin.look.data.dto

import com.google.gson.annotations.SerializedName

data class AuthRequest(
    @SerializedName("grant_type")
    val grantType: String = "social",
    val provider: String = "google",
    @SerializedName("client_id")
    val clientId: Int = 1,
    @SerializedName("client_secret")
    val clientSecret: String = "VrOQH6YzdR6QKBOS9ocd8GqCr1QKfSUAvniVbOUG",
    @SerializedName("access_token")
    val accessToken: String = ""
)