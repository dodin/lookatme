package com.dodin.look.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dodin.look.common.Lce
import com.dodin.look.data.dto.*
import com.dodin.look.data.local.Preferences
import com.dodin.look.data.local.db.AppDatabase
import com.dodin.look.data.mapper.PostMapper
import com.dodin.look.data.remote.Api
import com.dodin.look.data.remote.Oauth
import com.dodin.look.data.remote.PostSort
import com.dodin.look.ui.main.SearchDataSourceFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

typealias PostRepoResult = Pair<LiveData<PagedList<com.dodin.look.data.local.dto.Post>>, LiveData<Lce<Unit>>>

class Repository(private val api: Api,
                 private val auth: Oauth,
                 private val pref: Preferences,
                 private val db: AppDatabase,
                 private val mapper: PostMapper) {

    suspend fun login(authRequest: AuthRequest): Lce<Token> {
        return try {
            val response = api.login(authRequest)
            pref.token = response
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    fun getPosts(sort: Int): PostRepoResult {
        // get datasource from local cache
        val dataSourceFactory = when(sort) {
            PostSort.NEW.type -> db.postDao().getNew()
            PostSort.POPULAR.type -> db.postDao().getPopular()
            PostSort.FAVORITES.type -> db.postDao().getFavorites()
            PostSort.FEED.type -> db.postDao().getFeed()
            else -> db.postDao().getNew()
        }

        val boundaryCallback =
            PostsBoundaryCallback(sort, api, db, mapper)
        val networkErrors = boundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return (data to networkErrors)
    }

    fun search(query: String): PostRepoResult {
        val dataSourceFactory = SearchDataSourceFactory(query, api, GlobalScope, mapper)
        val networkErrors = dataSourceFactory.networkErrors
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .build()

        return (data to networkErrors)
    }

    suspend fun search(query: String, page: Int): Lce<List<Post>> {
        return try {
            val response = api.search(query, page)

            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun getPost(id: Int): Flow<Lce<Post>> = flow {
        try {
            val cache = db.postDao().findById(id)
            emit(Lce.Success(mapper.fromDb(cache)))

            val response = api.getPost(id)

            db.postDao().insert(mapper.toDb(response))
            db.commentDao().insertAll(mapper.commentsToDb(response))
            db.userDao().insertWithIgnore(mapper.commentsUsersToDb(response))

            emit(Lce.Success(response))
        } catch (e: Exception) {
            emit(Lce.Error(e))
        }
    }

    suspend fun getUserPosts(uid: Int, page: Int): Lce<List<Post>> {
        return try {
            val response = api.getUserPosts(uid, page)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun getUser(id: Int): Flow<Lce<User>> = flow {
        try {
            val cached = db.userDao().getUser(id)
            if (cached != null) {
                emit(Lce.Success(cached))
            }

            val response = api.getUser(id)
            db.userDao().insert(response)
            emit(Lce.Success(response))
        } catch (e: Exception) {
            emit(Lce.Error(e))
        }
    }

    suspend fun like(postId: Int): Flow<Lce<LikeResponse>> = flow {
        try {
            val userId = pref.user?.id ?: 0
            val post = db.postDao().findById(postId)
            post.like = !post.like
            val cacheResponse = LikeResponse(postId, userId, post.like)
            emit(Lce.Success(cacheResponse))
            db.postDao().update(post)

            val response = api.like(postId)
            emit(Lce.Success(response))
        } catch (e: Exception) {
            emit(Lce.Error(e))
        }
    }

    suspend fun subscribe(uid: Int): Lce<SubscribeResponse> {
        return try {
            val response = api.subscribe(uid)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun addComment(postId: Int, content: String): Flow<Lce<Comment>> = flow {
        try {
            val userId = pref.user?.id ?: 0
            val user = db.userDao().getUser(userId) ?: User()
            val tempComment = Comment(
                postId = postId,
                userId = userId,
                content = content,
                author = user)
            emit(Lce.Success(tempComment))

            val response = api.addComment(postId, CommentRequest(content))
            emit(Lce.Success(response))
        } catch (e: Exception) {
            emit(Lce.Error(e))
        }
    }

    suspend fun updatePhoto(file: File): Lce<User> {
        return try {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("photo", file.name, requestFile)
            val response = api.updatePhoto(body)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun updateProfile(profile: ProfileRequest): Lce<User> {
        return try {
            val response = api.updateProfile(profile)
            pref.user = response
            return Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun createPost(photos: List<File>, post: Post): Lce<Post> {
        return try {
            val photosParts = photos.map { file ->
                val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
                MultipartBody.Part.createFormData("photos[]", file.name, requestFile)
            }.toTypedArray()

            val response = api.createPost(photosParts, post)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun updatePost(photos: List<File>, post: Post): Lce<Post> {
        return try {
            val photosParts = photos.map { file ->
                val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
                MultipartBody.Part.createFormData("photos[]", file.name, requestFile)
            }.toTypedArray()

            val response = api.updatePost(post.id, photosParts, post)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun getSubscribers(): Lce<List<User>> {
        return try {
           val response = api.getSubscribers()
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun getSubscriptions(): Lce<List<User>> {
        return try {
            val response = api.getSubscriptions()
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun getMentions(query: String): Lce<List<User>> {
        return try {
            val response = api.mentions(query)
            Lce.Success(response)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    suspend fun updateFcmToken(token: String): Lce<Boolean> {
        return try {
            val response = api.updateFcmToken(token)
            Lce.Success(true)
        } catch (e: Exception) {
            Lce.Error(e)
        }
    }

    companion object {
        private const val DATABASE_PAGE_SIZE = 20
    }
}