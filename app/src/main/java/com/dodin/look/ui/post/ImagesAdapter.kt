package com.dodin.look.ui.post

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.dodin.look.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import jp.wasabeef.blurry.Blurry

class ImagesAdapter(
    private val context: Context,
    private var photos: List<String>,
    private val listener: ImageClickListener
) : PagerAdapter() {

    interface ImageClickListener {
        fun onImageClicked(images: List<String>, position: Int)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_slider_photo, container, false) as ViewGroup

        val imagePhoto = imageLayout.findViewById<ImageView>(R.id.imagePhoto)
        val imagePhotoBg = imageLayout.findViewById<ImageView>(R.id.imagePhotoBg)
        Picasso.get().load(photos[position]).into(imagePhoto, object : Callback {
            override fun onSuccess() {
                Blurry.with(context)
                    .async()
                    .from((imagePhoto.drawable as BitmapDrawable).bitmap)
                    .into(imagePhotoBg)
            }

            override fun onError(e: Exception?) {
            }
        })
        container.addView(imageLayout)

        imageLayout.setOnClickListener {
            listener.onImageClicked(photos, position)
        }
        return imageLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount() = photos.size

    /**
     * Update photos in adapter.
     * Will do nothing when submitting list that equals to current one.
     */
    fun submitList(images: List<String>) {
        if (photos == images) {
            return
        }

        photos = images
        notifyDataSetChanged()
    }
}