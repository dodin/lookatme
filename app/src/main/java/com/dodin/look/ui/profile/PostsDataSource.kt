package com.dodin.look.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.dodin.look.common.Lce
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.Post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PostsDataSource(private val uid: Int, private val repository: Repository, private val scope: CoroutineScope) : PageKeyedDataSource<Int, Post>() {
    private val firstPage = 1
    /**
     * Network state could be Loading, Error or Success
     */
    val networkState: MutableLiveData<Lce<Int>> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Post>) {
        fetchPage(firstPage) { data, _ ->
            callback.onResult(data, null, firstPage + 1)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {
        fetchPage(params.key) { data, page ->
            callback.onResult(data, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {
        fetchPage(params.key) { data, page ->
            callback.onResult(data, page - 1)
        }
    }

    private fun fetchPage(page: Int, callback: (data: List<Post>, currentPage: Int)->Unit) {
        networkState.postValue(Lce.Loading)
        scope.launch {
            val data = repository.getUserPosts(uid, page)
            when (data) {
                is Lce.Success -> {
                    networkState.postValue(Lce.Success(data.data.size))
                    callback.invoke(data.data, page)
                }
                is Lce.Error -> networkState.postValue(Lce.Error(data.message))
            }
        }
    }

}

class PostsDataSourceFactory(private val uid: Int, private val repository: Repository, private val scope: CoroutineScope): DataSource.Factory<Int, Post>() {
    val newsDataSourceLiveData = MutableLiveData<PostsDataSource>()

    override fun create(): DataSource<Int, Post> {
        val newsDataSource = PostsDataSource(uid, repository, scope)
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}