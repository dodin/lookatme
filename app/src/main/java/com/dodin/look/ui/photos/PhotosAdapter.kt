package com.dodin.look.ui.photos

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.davemorrissey.labs.subscaleview.ImageSource
import com.dodin.look.R
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class PhotosAdapter(val context: Context, val photos: List<String>) :
    PagerAdapter() {
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_slider_photo_zoom, container, false)

        val imagePhoto =
            imageLayout.findViewById<com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView>(R.id.imagePhoto)
        Picasso.get().load(photos[position]).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                if (bitmap != null)
                    imagePhoto.setImage(ImageSource.bitmap(bitmap))
            }
        })
        imagePhoto.maxScale = 10f
        container.addView(imageLayout)

        return imageLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount() = photos.size
}