package com.dodin.look.ui.subscribers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.dodin.look.common.CircleTransform
import com.dodin.look.data.dto.User
import com.squareup.picasso.Picasso


class SubscribersAdapter : ListAdapter<User, SubscriberViewHolder>(DIFF_CALLBACK) {
    var clickListener: SubscriberClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriberViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subscriber, parent, false)
        return SubscriberViewHolder(view)
    }

    override fun onBindViewHolder(holder: SubscriberViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}

class SubscriberViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val textName: TextView = view.findViewById(R.id.textName)
    private val imagePhoto: ImageView = view.findViewById(R.id.imagePhoto)
    private val buttonSubscribe: Button = view.findViewById(R.id.buttonSubscribe)
    private val buttonUnsubscribe: Button = view.findViewById(R.id.buttonUnsubscribe)

    fun bind(user: User, clickListener: SubscriberClickListener?) {
        val ctx = itemView.context
        textName.text = user.name
        Picasso.get().load(user.photo).transform(CircleTransform()).into(imagePhoto)
        buttonSubscribe.isVisible = !user.subscribed
        buttonUnsubscribe.isVisible = user.subscribed

        itemView.setOnClickListener {
            clickListener?.onClick(user)
        }
        buttonSubscribe.setOnClickListener {
            clickListener?.onSubscribe(user)
        }
        buttonUnsubscribe.setOnClickListener {
            clickListener?.onSubscribe(user)
        }
    }
}

interface SubscriberClickListener {
    fun onSubscribe(user: User)
    fun onClick(user: User)
}