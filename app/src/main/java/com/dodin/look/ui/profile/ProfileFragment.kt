package com.dodin.look.ui.profile

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider.getUriForFile
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.GridLayoutManager
import com.dodin.look.R
import com.dodin.look.common.AppUtils
import com.dodin.look.common.Lce
import com.dodin.look.common.showTitleCollapsed
import com.dodin.look.data.dto.User
import com.dodin.look.databinding.FragmentProfileBinding
import com.dodin.look.ui.auth.AuthActivity
import com.dodin.look.ui.main.PostsAdapter
import com.github.florent37.runtimepermission.RuntimePermission.askPermission
import com.yalantis.ucrop.UCrop
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import com.dodin.look.data.local.dto.Post as LocalPost

class ProfileFragment : Fragment(), PostsAdapter.ActionListener {
    private lateinit var binding: FragmentProfileBinding
    private val vm: ProfileViewModel by viewModel()
    private val args: ProfileFragmentArgs by navArgs()
    private val appUtils: AppUtils by inject()

    // Remember filename where camera saved the photo
    private var fileName: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(binding.collapsingToolbar, binding.toolbar, findNavController())

        val user = args.user
        vm.initUser(user)

        vm.user.observe(viewLifecycleOwner) {
            if (it is Lce.Success) {
                bindUser(it.data)
            }
        }

        binding.author.buttonSubscribe.setOnClickListener { subscribe() }
    }

    private fun bindUser(user: User) {
        // Show toolbar title only when collapsed
        binding.appbar.showTitleCollapsed(binding.collapsingToolbar, user.name)
        binding.user = user
        if (user.name.isEmpty()) {
            binding.author.textUserLogin.text = user.username
        }
        val isCurrentUser = appUtils.isCurrentUser(user)
        binding.author.textWebsiteLabel.movementMethod = LinkMovementMethod()
        binding.author.buttonEdit.isVisible = isCurrentUser
        binding.author.buttonCreateLook.isVisible = isCurrentUser
        binding.author.imageUserPhotoEdit.isVisible = isCurrentUser
        binding.author.imageUserPhoto.setOnClickListener { if (isCurrentUser) showPicker() }
        binding.author.imageUserPhotoEdit.setOnClickListener { if (isCurrentUser) showPicker() }
        binding.author.buttonEdit.setOnClickListener {
            if (appUtils.isAuthenticated()) {
                val action = ProfileFragmentDirections.editProfile(user)
                findNavController().navigate(action)
            }
        }

        // go to subscribers
        binding.author.textSubscribers.setOnClickListener {
            findNavController().navigate(R.id.showSubscribers)
        }
        binding.author.textSubscribersLabel.setOnClickListener {
            findNavController().navigate(R.id.showSubscribers)
        }

        // go to subscriptions
        binding.author.textSubscriptions.setOnClickListener {
            findNavController().navigate(R.id.showSubscribers)
        }
        binding.author.textSubscriptionsLabel.setOnClickListener {
            findNavController().navigate(R.id.showSubscribers)
        }

        binding.executePendingBindings()

        bindPosts(isCurrentUser)
    }

    private fun bindPosts(isCurrentUser: Boolean) {
        val adapter = PostsAdapter()
        adapter.listener = this
        binding.listPosts.layoutManager = GridLayoutManager(context, 2)
        binding.listPosts.adapter = adapter

        vm.getState().observe(viewLifecycleOwner, Observer {
            when (it) {
                is Lce.Success -> {
                    if (vm.getPosts()?.value?.isEmpty() == true && it.data == 0) {
                        if (isCurrentUser) {
                            binding.stateLayout.info().infoTitle(getString(R.string.profile_empty))
                                .infoButton(getString(R.string.profile_create), null)
                            binding.author.buttonCreateLook.isVisible = false
                        } else {
                            binding.stateLayout.info().infoTitle(getString(R.string.profile_user_empty))
                        }
                    } else {
                        binding.stateLayout.content()
                        if (isCurrentUser) {
                            binding.author.buttonCreateLook.isVisible = true
                        }
                    }
                }
                is Lce.Error -> binding.stateLayout.info().infoTitle(it.message)
                is Lce.Loading -> binding.stateLayout.loading()
            }
        })

//        vm.getPosts()?.observe(viewLifecycleOwner, Observer {
//            adapter.submitList(it)
//        })
    }

    override fun onItemClick(post: LocalPost, view: View) {
        ViewCompat.setTransitionName(view, "photo")
        val extras = FragmentNavigatorExtras(view to "photo")
        val args = Bundle().apply {
//            putParcelable("post", post)
            putInt("post_id", post.id)
        }
        findNavController().navigate(
            R.id.postFragment,
            args, // Bundle of args
            null, // NavOptions
            extras
        )
    }

    private fun showPicker() {
        with(AlertDialog.Builder(context)) {
            setTitle(getString(R.string.profile_picture_title))
            setItems(arrayOf(getString(R.string.camera), getString(R.string.gallery))) { _, index ->
                when (index) {
                    0 -> pictureFromCamera()
                    1 -> pictureFromGallery()
                }
            }
            create().show()
        }
    }

    private fun pictureFromCamera() {
        askPermission(this, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).ask {
            if (it.isAccepted) {
                val ctx = context ?: return@ask

                fileName = System.currentTimeMillis().toString() + ".jpg"
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName))
                if (takePictureIntent.resolveActivity(ctx.packageManager) != null) {
                    startActivityForResult(takePictureIntent, RC_IMAGE_CAMERA)
                }
            }
        }
    }

    private fun pictureFromGallery() {
        val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, RC_IMAGE_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_IMAGE_GALLERY -> {
                if (resultCode == RESULT_OK) {
                    val imageUri = data?.data ?: return
                    cropImage(imageUri)
                }
            }
            RC_IMAGE_CAMERA -> {
                if (resultCode == RESULT_OK) {
                    val imageUri = getCacheImagePath(fileName)
                    cropImage(imageUri)
                }
            }
            UCrop.REQUEST_CROP -> {
                if (resultCode == RESULT_OK) {
                    handleUCropResult(data)
                }
            }
            AuthActivity.RC_AUTH -> {
                if (resultCode == Activity.RESULT_OK) {
                    subscribe()
                }
            }
        }
    }

    private fun cropImage(sourceUri: Uri) {
        val ctx = context ?: return
        val destinationUri = Uri.fromFile(File(ctx.cacheDir, fileName))
        val options = UCrop.Options()
        options.setCompressionQuality(90)

        UCrop.of(sourceUri, destinationUri)
            .withOptions(options)
            .start(ctx, this)
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            return
        }
        val resultUri = UCrop.getOutput(data) ?: return
        vm.updateProfilePhoto(resultUri)
    }

    private fun getCacheImagePath(fileName: String): Uri {
        val path = File(context?.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return getUriForFile(context!!, context?.packageName + ".provider", image)
    }

    private fun subscribe() {
        if (appUtils.isAuthenticated()) {
            vm.subscribe()
        } else {
            startActivityForResult(AuthActivity.intent(context!!, SUBSCRIBE), AuthActivity.RC_AUTH)
        }
    }

    companion object {
        const val RC_IMAGE_CAMERA = 112
        const val RC_IMAGE_GALLERY = 113
        const val RC_CROP = 114
        const val SUBSCRIBE = "subscribe"
    }
}