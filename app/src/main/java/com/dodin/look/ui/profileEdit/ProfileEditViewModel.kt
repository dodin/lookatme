package com.dodin.look.ui.profileEdit

import com.dodin.look.common.CoroutineViewModel
import com.dodin.look.common.Lce
import com.dodin.look.common.ViewStateStore
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.ProfileRequest
import com.dodin.look.data.dto.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileEditViewModel(private val repo: Repository) : CoroutineViewModel() {
    val state: ViewStateStore<Lce<Action>> = ViewStateStore(Lce.Success(Action.Pending()))

    fun updateProfile(name: String, username: String, about: String, website: String) = launch(Dispatchers.Main) {
        if (username.isEmpty()) {
            state.dispatchState(Lce.Error("Empty username"))
            return@launch
        }

        val response = repo.updateProfile(ProfileRequest(name, username, about, website))
        if (response is Lce.Success) {
            state.dispatchState(Lce.Success(Action.Updated(response.data)))
        } else if (response is Lce.Error) {
            state.dispatchState(response)
        }
    }
}

sealed class Action {
    class Pending : Action()
    class Updated(public val user: User) : Action()
}