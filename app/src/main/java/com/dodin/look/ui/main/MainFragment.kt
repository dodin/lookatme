package com.dodin.look.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.GridLayoutManager
import com.dodin.look.R
import com.dodin.look.common.AppUtils
import com.dodin.look.common.Lce
import com.dodin.look.common.ifAuthenticated
import com.dodin.look.common.observe
import com.dodin.look.data.remote.PostSort
import com.dodin.look.databinding.FragmentMainBinding
import com.dodin.look.ui.auth.AuthActivity
import com.erkutaras.statelayout.StateLayout
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.dodin.look.data.local.dto.Post as LocalPost

class MainFragment : Fragment(), PostsAdapter.ActionListener {
    private lateinit var binding: FragmentMainBinding
    private val vm: MainViewModel by viewModel()
    private val appUtils: AppUtils by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())

        // Setup toolbar menu
        binding.toolbar.inflateMenu(R.menu.main_filter)
        binding.toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.feedPosts -> fetchPosts(PostSort.FEED)
                R.id.newPosts -> fetchPosts(PostSort.NEW)
                R.id.popularPosts -> fetchPosts(PostSort.POPULAR)
                R.id.favorites -> fetchPosts(PostSort.FAVORITES)
            }
            return@setOnMenuItemClickListener true
        }

        // Listen for search events
        val search = binding.toolbar.menu.getItem(0).actionView as SearchView
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { vm.search(it) }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { vm.search(it) }
                return true
            }
        })

        // Setup bottom navigation
        binding.navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> resetFilter()
                R.id.profile -> openProfile()
                R.id.createPost -> createPost()
                else -> NavigationUI.onNavDestinationSelected(it, findNavController())
            }
            true
        }

        // Setup swipe to refresh
        binding.swipeRefresh.setOnRefreshListener {
            fetchPosts()
        }

        val q = arguments?.getString("query")
        if (q != null && q.isNotEmpty()) {
            binding.toolbar.menu.getItem(0).expandActionView()
            search.setIconifiedByDefault(false)
            search.setIconified(false)
            search.setQuery(q, true)
        }
        initAdapter()

        vm.events.observe(viewLifecycleOwner) {
            if (it is SearchStarted) {
                resetList()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AuthActivity.RC_AUTH && resultCode == Activity.RESULT_OK) {
            val referrer = data?.getStringExtra(AuthActivity.REFERRER)
            when (referrer) {
                OPEN_PROFILE -> openProfile()
                CREATE_POST -> createPost()
            }
        }
    }

    /**
     * Adapter item click listener
     * @see {@link cPostAdapter}
     */
    override fun onItemClick(post: LocalPost, view: View) {
        ViewCompat.setTransitionName(view, "photo")
        val extras = FragmentNavigatorExtras(view to "photo")
        val args = Bundle().apply {
            putInt("post_id", post.id)
        }
        findNavController().navigate(
            R.id.showPost,
            args, // Bundle of args
            null, // NavOptions
            extras
        )
    }

    /**
     * Initiate adapter and event listeners
     */
    private fun initAdapter() {
        // Init list
        binding.listPosts.layoutManager = GridLayoutManager(context, 2)
        resetList()

        // Observe data
        vm.getPosts().observe(viewLifecycleOwner, Observer { pagedList ->
            (binding.listPosts.adapter as PostsAdapter).submitList(pagedList)
        })

        // Refresh listener for 'Refresh' button when error showing
        val refreshListener = object : StateLayout.OnStateLayoutListener {
            override fun onStateLayoutInfoButtonClick() {
                fetchPosts()
            }
        }

        // Listen to state
        vm.networkErrors.observe(viewLifecycleOwner) { networkState ->
            when (networkState) {
                is Lce.Success -> {
                    binding.swipeRefresh.isRefreshing = false
                    binding.stateLayout.content()
                }
                is Lce.Error -> {
                    binding.swipeRefresh.isRefreshing = false

                    // Show small notice about error when there are some posts cached
                    if (binding.listPosts.adapter!!.itemCount > 0) {
                        binding.stateLayout.content()
                        Snackbar.make(binding.root, networkState.message, Snackbar.LENGTH_SHORT).show()
                        return@observe
                    }

                    // Show big error in other case
                    binding.stateLayout.info()
                        .infoTitle(networkState.message)
                        .infoButton(getString(R.string.refresh), refreshListener)
                }
                is Lce.Loading -> binding.stateLayout.loading()
            }
        }
    }

    private fun resetList() {
        binding.listPosts.adapter = PostsAdapter().apply {
            listener = this@MainFragment
        }
    }

    private fun fetchPosts(sort: PostSort? = null) {
        vm.fetchPosts(sort)
        initAdapter()
    }

    private fun resetFilter() {
        fetchPosts(PostSort.NEW)
    }

    private fun openProfile() {
        ifAuthenticated(appUtils, OPEN_PROFILE) {
            val directions = MainFragmentDirections.showProfile(null)
            findNavController().navigate(directions)
        }
    }

    private fun createPost() {
        ifAuthenticated(appUtils, CREATE_POST) {
            findNavController().navigate(R.id.postCreateFragment)
        }
    }

    companion object {
        const val OPEN_PROFILE = "open_profile"
        const val CREATE_POST = "create_post"
    }
}