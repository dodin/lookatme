package com.dodin.look.ui.auth

import com.dodin.look.common.CoroutineViewModel
import com.dodin.look.common.Lce
import com.dodin.look.common.ViewStateStore
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.AuthRequest
import com.dodin.look.data.local.Preferences
import com.dodin.look.usecases.UpdateFcmUseCase
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class AuthViewModel(private val repo: Repository, private val pref: Preferences) : CoroutineViewModel() {
    val state: ViewStateStore<Lce<Action>> = ViewStateStore(Lce.Loading)

    init {
        if (pref.token != null) {
            state.dispatchState(Lce.Success(Action.Authorized()))
        } else {
            state.dispatchState(Lce.Success(Action.StartAuthorization()))
        }
    }

    fun send(accessToken: String) = launch(Dispatchers.Main) {
        try {
            val response = repo.login(AuthRequest(accessToken = accessToken))
            if (response is Lce.Success) {
                pref.token = response.data
                sendFcm()
                state.dispatchState(Lce.Success(Action.Authorized()))
            } else if (response is Lce.Error ){
                state.dispatchState(Lce.Error(response.message))
            }
        } catch (e: Exception) {
            state.dispatchState(Lce.Error(e))
        }
    }

    private fun sendFcm() {
        val updateFcmUseCase = UpdateFcmUseCase(repo, pref)
        updateFcmUseCase.run()
    }
}

sealed class Action {
    class StartAuthorization : Action()
    class Authorized : Action()
}