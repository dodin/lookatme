package com.dodin.look.ui.profileEdit

import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.dodin.look.R
import com.dodin.look.common.Lce
import com.dodin.look.databinding.FragmentProfileEditBinding
import com.dodin.look.ui.profile.ProfileFragmentArgs
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileEditFragment : Fragment() {
    lateinit var binding: FragmentProfileEditBinding
    private val vm: ProfileEditViewModel by viewModel()
    private val args: ProfileFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_edit, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())
        setHasOptionsMenu(true)
        binding.toolbar.inflateMenu(R.menu.apply)
        binding.toolbar.setOnMenuItemClickListener {
            menuItemSelected(it)
            true
        }

        binding.user = args.user
        // Filter username.
        val allowedCharacters = Regex("[a-zA-Z0-9.]+")
        val filter = InputFilter { source, start, end, dest, dstart, dend ->
            if (source != null && !allowedCharacters.matches(source)) {
                return@InputFilter ""
            }
            return@InputFilter null
        }
        binding.editUsername.filters = arrayOf(filter)

        vm.state.observe(viewLifecycleOwner) {
            when(it) {
                is Lce.Success -> {
                    if (it.data is Action.Updated) {
                        findNavController().popBackStack()
                    }
                }
                is Lce.Error -> {
                    Snackbar.make(view.rootView, it.message, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun menuItemSelected(item: MenuItem?) {
        if (item?.itemId == R.id.item_apply) {
            vm.updateProfile(
                binding.editName.text.toString(),
                binding.editUsername.text.toString(),
                binding.editAbout.text.toString(),
                binding.editWebsite.text.toString())
        }
    }
}