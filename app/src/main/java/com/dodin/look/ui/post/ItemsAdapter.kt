package com.dodin.look.ui.post

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dodin.look.R
import com.dodin.look.common.linkify
import com.dodin.look.common.openUrl
import com.dodin.look.data.dto.Item

class ItemsAdapter : ListAdapter<Item, ItemViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.title == newItem.title
            }
        }
    }
}

class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val textName: TextView = view.findViewById(R.id.textName)
    private val textSize: TextView = view.findViewById(R.id.textSize)
    private val textColor: TextView = view.findViewById(R.id.textColor)

    fun bindTo(item: Item) {
        val context = itemView.context

        textName.text = item.title
        textSize.text = context.getString(R.string.size, item.size)
        textColor.text = context.getString(R.string.color, item.color)

        if (item.link.isNotBlank()) {
            textName.linkify()
            itemView.setOnClickListener {
                openUrl(itemView.context, item.link)
            }
        }
    }
}