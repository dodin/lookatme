package com.dodin.look.ui.postCreate

import android.net.Uri
import com.dodin.look.common.CoroutineViewModel
import com.dodin.look.common.Lce
import com.dodin.look.common.ViewStateStore
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.Item
import com.dodin.look.data.dto.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class PostCreateViewModel(private val repo: Repository) : CoroutineViewModel() {
    val state: ViewStateStore<Lce<Action>> = ViewStateStore(Lce.Success(Action.New()))

    var post = Post()

    fun createPost(post: Post) = launch(Dispatchers.Main) {
        state.dispatchState(Lce.Loading)
        // Get local file into this list
        val files = post.photos.filter { !it.startsWith("http") }.map { File(it) }

        // Create or update post depending on post id
        val result = if (post.id == 0) repo.createPost(files, post) else repo.updatePost(files, post)

        when (result) {
            is Lce.Success -> state.dispatchState(Lce.Success(Action.Created()))
            is Lce.Error -> state.dispatchState(result)
        }
    }

    fun removePhoto(position: Int) {
        post = post.copy(
            photos = post.photos.toMutableList().apply { removeAt(position) }
        )
    }

    fun setPhotos(uris: List<Uri>) {
        post = post.copy(
            photos = uris.map { it.toString() }
        )
    }

    fun addPhotos(uris: List<Uri>) {
        post = post.copy(
            photos = post.photos + uris.map { it.toString() }
        )
    }

    fun removeItem(position: Int) {
        post = post.copy(
            items = post.items.toMutableList().apply { removeAt(position) }
        )
    }

    fun setItems(items: List<Item>) {
        post = post.copy(
            items = items
        )
    }
}

sealed class Action {
    class Created : Action()
    class New : Action()
}