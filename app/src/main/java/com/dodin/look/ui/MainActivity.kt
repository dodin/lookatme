package com.dodin.look.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.dodin.look.R
import com.dodin.look.data.Repository
import com.dodin.look.data.local.Preferences
import com.dodin.look.usecases.UpdateFcmUseCase
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    val repository: Repository by inject()
    val preferences: Preferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val updateFcmUseCase = UpdateFcmUseCase(repository, preferences)
        updateFcmUseCase.run()
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_fragment).navigateUp()
}
