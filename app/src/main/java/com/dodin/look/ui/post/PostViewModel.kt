package com.dodin.look.ui.post

import com.dodin.look.common.*
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.Comment
import com.dodin.look.data.dto.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.dodin.look.common.mentionadapter.MentionPerson
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay

class PostViewModel(private val repo: Repository, private val appUtils: AppUtils) : CoroutineViewModel() {
    val comments = ViewStateStore<Lce<List<Comment>>>(Lce.Loading)
    val post = ViewStateStore<Lce<Post>>(Lce.Loading)
    val mentions = ViewStateStore<Lce<List<MentionPerson>>>(Lce.Loading)

    val tempComments = arrayListOf<Comment>()
    var isCurrentUser = false

    /**
     * Keep mention job here. Used for debounce
     */
    var mentionsJob: Job = Job()

    fun loadPost(id: Int) = launch(Dispatchers.Main) {
        repo.getPost(id).collect {
            if (it is Lce.Success) {
                isCurrentUser = appUtils.isCurrentUser(it.data.author)
                post.dispatchState(it)
                comments.dispatchState(Lce.Success(it.data.comments))
            }
        }
    }

    fun fetchComments(id: Int) = launch(Dispatchers.Main) {
//        // Do not reload existing data
//        if (comments.state() is Lce.Success) {
//            return@launch
//        }
//
//        val post = repo.getPost(id)
//        if (post is Lce.Success) {
//            comments.dispatchState(Lce.Success(post.data.comments))
//        } else if (post is Lce.Error){
//            comments.dispatchState(post)
//        }
    }

    fun like() = launch(Dispatchers.Main) {
        val postState = post.state()
        if (postState is Lce.Success) {
            val response = repo.like(postState.data.id)
            response.collect {
                if (it is Lce.Success) {
                    val newPostState = postState.data.copy(like = it.data.like)
                    post.dispatchState(Lce.Success(newPostState))
                }
            }
        }
    }

    fun addComment(content: String) = launch(Dispatchers.Main) {
        if (content.trim().isEmpty()) {
            return@launch
        }

        val postState = post.state() as? Lce.Success ?: return@launch

        repo.addComment(postState.data.id, content).collect {
            if (it is Lce.Success) {
                if (comments.state() is Lce.Success) {
                    val currentComments = comments.state().data ?: listOf()
                    val newComments = listOf(it.data) + currentComments

                    val filteredComments = replaceTempComment(newComments, tempComments)
                    if (it.data.id == 0) {
                        tempComments.add(it.data)
                    }
                    comments.dispatchState(Lce.Success(filteredComments))
                } else {
                    comments.dispatchState(Lce.Success(listOf(it.data)))
                }
            }
        }
    }

    /**
     * Fetch mention suggestions debounced
     */
    fun fetchMentions(query: String) {
        if (query.isEmpty()) {
            return
        }

        mentionsJob = debounceJob(query, mentionsJob) {
            val response = repo.getMentions("@$query")
            if (response is Lce.Success) {
                val persons = response.data.map {
                    MentionPerson(it.username, it.name, it.id.toString(), it.photo)
                }
                mentions.dispatchState(Lce.Success(persons))
            }
        }
    }
}