package com.dodin.look.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.dodin.look.common.Lce
import com.dodin.look.data.local.dto.Post
import com.dodin.look.data.mapper.PostMapper
import com.dodin.look.data.remote.Api
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * Data source that allows to make a search api call
 */
class SearchDataSource(
    private val query: String,
    private val api: Api,
    private val scope: CoroutineScope,
    private val mapper: PostMapper
) : PageKeyedDataSource<Int, Post>() {

    private val firstPage = 1

    /**
     * Network state could be Loading, Error or Success
     */
    val networkErrors: MutableLiveData<Lce<Unit>> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Post>
    ) {
        fetchPage(query, firstPage) { data, _ ->
            callback.onResult(data, null, firstPage + 1)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {
        fetchPage(query, params.key) { data, page ->
            callback.onResult(data, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {
        fetchPage(query, params.key) { data, page ->
            callback.onResult(data, page - 1)
        }
    }

    private fun fetchPage(
        query: String,
        page: Int,
        callback: (data: List<Post>, currentPage: Int) -> Unit
    ) {
        networkErrors.postValue(Lce.Loading)

        scope.launch {
            try {
                val data = api.search(query, page).map { mapper.toDb(it) }
                networkErrors.postValue(Lce.Success(Unit))
                callback.invoke(data, page)
            } catch (e: Exception) {
                networkErrors.postValue(Lce.Error(e.localizedMessage))
            }
        }
    }
}

class SearchDataSourceFactory(
    private val query: String,
    private val api: Api,
    private val scope: CoroutineScope,
    private val mapper: PostMapper
) : DataSource.Factory<Int, Post>() {

    val newsDataSourceLiveData = MutableLiveData<SearchDataSource>()
    var networkErrors: MutableLiveData<Lce<Unit>> = MutableLiveData()

    override fun create(): DataSource<Int, Post> {
        val newsDataSource = SearchDataSource(query, api, scope, mapper)
        networkErrors = newsDataSource.networkErrors
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}
