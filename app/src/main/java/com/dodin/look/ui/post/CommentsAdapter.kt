package com.dodin.look.ui.post

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.dodin.look.common.CircleTransform
import com.dodin.look.data.dto.Comment
import com.squareup.picasso.Picasso

class CommentsAdapter : ListAdapter<Comment, CommentViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Comment>() {
            override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
                return oldItem.content == newItem.content
            }
        }
    }
}

class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val textContent: TextView = view.findViewById(R.id.textContent)
    private val imagePhoto: ImageView = view.findViewById(R.id.imagePhoto)
    private val textTime: TextView = view.findViewById(R.id.textTime)

    fun bindTo(item: Comment) {
        val context = itemView.context

        val rawComment = context.getString(R.string.comment_content, item.author.name, item.content)
        val formattedComment = HtmlCompat.fromHtml(rawComment, HtmlCompat.FROM_HTML_MODE_LEGACY)
        textContent.text = formattedComment

        textTime.text = DateUtils.getRelativeTimeSpanString(context, item.createdAt.time)

        if (item.author.photo.isNotEmpty()) {
            Picasso.get().load(item.author.photo).transform(CircleTransform()).into(imagePhoto)
        }
    }
}