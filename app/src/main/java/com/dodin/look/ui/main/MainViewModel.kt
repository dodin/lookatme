package com.dodin.look.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import com.dodin.look.common.*
import com.dodin.look.data.Repository
import com.dodin.look.data.remote.PostSort
import kotlinx.coroutines.*
import com.dodin.look.data.local.dto.Post as LocalPost

class MainViewModel(private val repository: Repository) : CoroutineViewModel() {
    /**
     * Observable posts livadata for list
     */
    val postsLiveData: MutableLiveData<LiveData<PagedList<LocalPost>>> = MutableLiveData()
    // Hold network errors in this LiveData
    lateinit var networkErrors: LiveData<Lce<Unit>>

    val events = Events.Emitter()

    /**
     * Remember last sort order so we can repeat in case of error
     */
    private var lastSortOrder: PostSort = PostSort.NEW

    private var searchJob: Job = Job()

    init {
        fetchPosts(PostSort.NEW)
    }

    fun getPosts(): LiveData<PagedList<LocalPost>> = Transformations.switchMap(postsLiveData) {
        it
    }

    /**
     * Create data source to fetch posts
     */
    fun fetchPosts(sort: PostSort? = null) {
        if (sort != null) {
            lastSortOrder = sort
        }

        val (data, errors) = repository.getPosts(lastSortOrder.type)
        postsLiveData.value = data
        networkErrors = errors
    }

    /**
     * Debounce search request
     */
    fun search(query: String) {
        searchJob = debounceJob(query, searchJob, ::fetchSearch)
    }

    private suspend fun fetchSearch(query: String) {
        if (query.isEmpty()) {
            return
        }

        val (data, errors) = repository.search(query)
        postsLiveData.value = data
        networkErrors = errors
        events.emitAndExecuteOnce(SearchStarted())
    }
}

class SearchStarted : NavigationEvent()