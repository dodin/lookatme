package com.dodin.look.ui.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dodin.look.BuildConfig
import com.dodin.look.R
import com.dodin.look.common.Lce
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class AuthActivity : AppCompatActivity() {
    private val vm: AuthViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        setResult(Activity.RESULT_CANCELED)

        vm.state.observe(this) {
            when (it) {
                is Lce.Success -> {
                    when (it.data) {
                        is Action.StartAuthorization -> auth()
                        is Action.Authorized -> close()
                    }
                }
                is Lce.Error -> {
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_AUTH && resultCode == Activity.RESULT_OK) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    sendToBackend(account)
                }
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Timber.w("signInResult:failed code=" + e.statusCode)
            }

        } else {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun close() {
        // send referrer back
        val referrer = intent.getStringExtra(REFERRER)
        val intent = Intent().apply {
            putExtra(REFERRER, referrer)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun auth() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestServerAuthCode("908453654213-rdd4pk16iaav3agqvdvsu8b3o85eo5f7.apps.googleusercontent.com")
//            .requestIdToken("908453654213-rdd4pk16iaav3agqvdvsu8b3o85eo5f7.apps.googleusercontent.com")
            .requestIdToken(BuildConfig.GOOLE_CLIENT_ID)
            .requestEmail()
            .requestProfile()
            .build()
        val googleSignInClient = GoogleSignIn.getClient(this, gso)
//        val account = GoogleSignIn.getLastSignedInAccount(this)
//        if (account == null) {
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_AUTH)
//        } else {
//            sendToBackend(account)
//        }
    }

    private fun sendToBackend(account: GoogleSignInAccount) {
        val serverAuthCode = account.idToken
        if (serverAuthCode != null) {
            vm.send(serverAuthCode)
        } else {
            finish()
        }
        // Get auth token for account. Deprecated
//        GlobalScope.launch {
//            val token = GlobalScope.async {
//                GoogleAuthUtil.getToken(
//                    applicationContext,
//                    account,
//                    "oauth2:" + Scopes.PROFILE
//                )
//            }.await()
//            vm.send(token)
//        }
    }

    companion object {
        const val RC_AUTH = 999
        const val REFERRER = "referrer"

        fun intent(ctx: Context, referrer: String) = Intent(ctx, AuthActivity::class.java).apply {
            putExtra(REFERRER, referrer)
        }
    }
}