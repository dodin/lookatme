package com.dodin.look.ui.postCreate.itemAdd

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.dodin.look.R
import com.dodin.look.common.hideKeyboard
import com.dodin.look.data.dto.Item
import com.dodin.look.databinding.ActivityItemAddBinding

class AddItemActivity : AppCompatActivity() {
    private lateinit var binding: ActivityItemAddBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_item_add)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val item = intent.getParcelableArrayListExtra<Item>(PARAM_ITEM)
        val position = intent.getIntExtra(PARAM_POSITION, 0)

        initUi(item, position)
        setResult(Activity.RESULT_CANCELED)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.apply, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId ) {
            R.id.item_apply -> {
                val resultIntent = Intent().apply {
                    putParcelableArrayListExtra(PARAM_ITEM, getItems())
                    putExtra(PARAM_POSITION, intent.getIntExtra(PARAM_POSITION, 0))
                }
                setResult(Activity.RESULT_OK, resultIntent)
                hideKeyboard()
                finish()
            }
            android.R.id.home -> {
                hideKeyboard()
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initUi(items: ArrayList<Item>, position: Int) {
        binding.list.layoutManager = LinearLayoutManager(this)
        binding.list.setHasFixedSize(true)
        binding.list.adapter = FormAdapter(items)
        binding.list.smoothScrollToPosition(position)
    }

    private fun getItems(): ArrayList<Item> {
        return (binding.list.adapter as FormAdapter).elements as ArrayList<Item>
    }

    companion object {
        const val PARAM_ITEM = "item"
        const val PARAM_POSITION = "position"

        fun getIntent(ctx: Context, items: ArrayList<Item> = arrayListOf(Item()), position: Int = 0): Intent {
            return Intent(ctx, AddItemActivity::class.java).apply {
                putParcelableArrayListExtra(PARAM_ITEM, items)
                putExtra(PARAM_POSITION, position)
            }
        }
    }
}