package com.dodin.look.ui.postCreate

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.dodin.look.R
import com.dodin.look.common.Lce
import com.dodin.look.common.queryPath
import com.dodin.look.common.ui.createProgressDialog
import com.dodin.look.data.dto.Item
import com.dodin.look.data.dto.Post
import com.dodin.look.databinding.FragmentPostCreateBinding
import com.dodin.look.ui.postCreate.itemAdd.AddItemActivity
import com.github.florent37.runtimepermission.RuntimePermission.askPermission
import com.google.android.material.snackbar.Snackbar
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import org.koin.androidx.viewmodel.ext.android.viewModel


class PostCreateFragment : Fragment() {
    private lateinit var binding: FragmentPostCreateBinding
    private val vm: PostCreateViewModel by viewModel()
    private val progress by lazy { createProgressDialog(context!!) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_create, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initEvents()

        initPhotos()
        initItems()

        val post = arguments?.getParcelable<Post>(ARG_POST)
        if (post != null) {
            vm.post = post.copy(comments = listOf()) // No need to store comments
            bindPost(post)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // User picked some photos
        if (requestCode == RC_IMAGE_GALLERY && resultCode == RESULT_OK) {
            val imageUris = Matisse.obtainResult(data)
            vm.addPhotos(imageUris)
            (binding.listPhotos.adapter as PhotosAdapter).submitList(vm.post.photos.map { Uri.parse(it) })
        }

        // User added/updated items
        if (requestCode == RC_EDIT_ITEMS && resultCode == RESULT_OK) {
            val elements = data?.getParcelableArrayListExtra("item") ?: arrayListOf(Item())
            vm.setItems(elements)
            (binding.listItems.adapter as ItemsAdapter).submitList(elements)
            (binding.listItems.adapter as ItemsAdapter).notifyDataSetChanged()

            updateAddEditButton(elements)
        }
    }

    private fun initUi() {
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())
        setHasOptionsMenu(true)
        binding.toolbar.inflateMenu(R.menu.apply)
        // Save button
        binding.toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.item_apply) {
                savePost()
            }
            true
        }
        // Add/Edit items button
        binding.buttonAddItem.setOnClickListener {
            val items = if (vm.post.items.isNotEmpty()) vm.post.items as ArrayList else arrayListOf(Item())
            startActivityForResult(AddItemActivity.getIntent(context!!, items), RC_EDIT_ITEMS)
        }
        //Add photos button
        binding.buttonAddPhoto.setOnClickListener {
            askPermission(this@PostCreateFragment, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).ask {
                if (!it.isAccepted) {
                    return@ask
                }

                Matisse.from(this)
                    .choose(MimeType.ofImage())
                    .maxSelectable(9)
                    .countable(true)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(MyPicassonEngine())
                    .forResult(RC_IMAGE_GALLERY)
            }

        }
    }

    private fun initEvents() {
        vm.state.observe(viewLifecycleOwner) {
            // Hide progress dialog if state is not loading
            if (it !is Lce.Loading && progress.isShowing) {
                progress.dismiss()
            }

            when(it) {
                is Lce.Loading -> progress.show()
                is Lce.Error -> Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG).show()
                is Lce.Success -> {
                    if (it.data is Action.Created) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }

    /**
     * Init photos list and click listener
     */
    private fun initPhotos() {
        val listener = object : PhotoListener {
            override fun removeClicked(position: Int) {
                vm.removePhoto(position)
                with((binding.listPhotos.adapter as PhotosAdapter)) {
                    submitList(vm.post.photos.map { Uri.parse(it) })
                    notifyDataSetChanged()
                }
            }

            override fun itemClicked(position: Int) {

            }
        }
        val photosAdapter = PhotosAdapter(listener)
        binding.listPhotos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.listPhotos.adapter = photosAdapter
    }

    /**
     * Init items list and click listener
     */
    private fun initItems() {
        val listener = object : PhotoListener {
            override fun removeClicked(position: Int) {
                vm.removeItem(position)
                with((binding.listItems.adapter as ItemsAdapter)) {
                    submitList(vm.post.items)
                    notifyDataSetChanged()
                }

                updateAddEditButton(vm.post.items)
            }

            override fun itemClicked(position: Int) {
                startActivityForResult(AddItemActivity.getIntent(context!!, vm.post.items as ArrayList<Item>, position), RC_EDIT_ITEMS)
            }
        }
        val itemsAdapter = ItemsAdapter(listener)
        binding.listItems.layoutManager = LinearLayoutManager(context)
        binding.listItems.adapter = itemsAdapter
    }

    private fun bindPost(post: Post) {
        binding.editTitle.setText(post.title)
        binding.editDescription.setText(post.description)
        (binding.listItems.adapter as ItemsAdapter).submitList(vm.post.items)
        (binding.listPhotos.adapter as PhotosAdapter).submitList(vm.post.photos.map { Uri.parse(it) })
        updateAddEditButton(post.items)
    }

    // Validate data and save post
    private fun savePost() {
        val ctx = context ?: return
        val rootView = view?.rootView ?: return

        // Prepare post
        val look = vm.post.copy(
            title = binding.editTitle.text.toString().trim(),
            description = binding.editDescription.text.toString().trim(),
            photos = vm.post.photos.map {
                return@map if (it.startsWith("http")) {
                    it
                } else {
                    ctx.contentResolver.queryPath(Uri.parse(it))
                }
            }
        )

        // validate
        if (look.title.isEmpty()) {
            Snackbar.make(rootView, R.string.post_create_empty_title, Snackbar.LENGTH_SHORT).show()
            return
        }
        if (look.items.isEmpty()) {
            Snackbar.make(rootView, R.string.post_create_empty_items, Snackbar.LENGTH_SHORT).show()
            return
        }
        if (look.photos.isEmpty()) {
            Snackbar.make(rootView, R.string.post_create_empty_photos, Snackbar.LENGTH_SHORT).show()
            return
        }

        vm.createPost(look)
    }

    /**
     * Set button title depending on items count
     */
    private fun updateAddEditButton(elements: Collection<Item>) {
        binding.buttonAddItem.setText(
            if (elements.isNotEmpty()) {
                R.string.post_create_edit_item
            } else {
                R.string.post_create_add_item
            }
        )
    }

    companion object {
        const val RC_IMAGE_GALLERY = 113
        const val RC_EDIT_ITEMS = 122
        const val ARG_POST = "post"
    }
}