package com.dodin.look.ui.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dodin.look.common.AppUtils
import com.dodin.look.common.CoroutineViewModel
import com.dodin.look.common.Lce
import com.dodin.look.common.ViewStateStore
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.Post
import com.dodin.look.data.dto.User
import com.dodin.look.data.local.Preferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File

class ProfileViewModel(private val repo: Repository, private val pref: Preferences,  val appUtils: AppUtils) : CoroutineViewModel() {
    val user: ViewStateStore<Lce<User>> = ViewStateStore(Lce.Loading)
    private val pageSize = 10
    private lateinit var postsLivedata: LiveData<PagedList<Post>>
    private lateinit var postsDataFactory: PostsDataSourceFactory

    fun getPosts(): LiveData<PagedList<Post>>? = postsLivedata

    fun initUser(data: User?) {
        if (user.state() !is Lce.Success) {
            if (data != null) {
                fetchUser(data.id)
                fetchUserPosts(data.id)
                user.dispatchState(Lce.Success(data))
            } else {
                val userId = appUtils.getCurrentUserId()
                pref.user?.let { user.dispatchState(Lce.Success(it)) }
                fetchUser(userId)
                fetchUserPosts(userId)
            }
        } else {
            pref.user?.let { user.dispatchState(Lce.Success(it)) }
        }
    }

    fun getState(): LiveData<Lce<Int>> = Transformations.switchMap<PostsDataSource,
            Lce<Int>>(postsDataFactory.newsDataSourceLiveData, PostsDataSource::networkState)

    private fun fetchUser(id: Int) = launch(Dispatchers.Main) {
        val userResp = repo.getUser(id)

        userResp.collect {
            if (it is Lce.Success) {
                user.dispatchState(it)
            }
        }
    }

    private fun fetchUserPosts(id: Int) {
        postsDataFactory = PostsDataSourceFactory(id, repo, this)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        postsLivedata = LivePagedListBuilder<Int, Post>(postsDataFactory, config).build()
    }

    fun subscribe() = launch(Dispatchers.Main) {
        val state = user.state()
        if (state is Lce.Success) {
            val userObj = state.data
            val response = repo.subscribe(userObj.id)
            if (response is Lce.Success) {
                val subscribers = userObj.subscribers
                val newSubscribers = if (response.data.subscribed)
                    subscribers + 1
                else
                    subscribers - 1

                user.dispatchState(
                    Lce.Success(
                        userObj.copy(
                            subscribers = newSubscribers,
                            subscribed = response.data.subscribed
                        )
                    )
                )
            }
        }
    }

    fun updateProfilePhoto(uri: Uri) = launch(Dispatchers.Main) {
        val file = File(uri.path)
        val response = repo.updatePhoto(file)
        if (response is Lce.Success) {
            user.dispatchState(response)
        }
    }
}