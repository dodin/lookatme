package com.dodin.look.ui.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dodin.look.R
import com.dodin.look.databinding.FragmentPhotosBinding

class PhotosFragment : Fragment() {
    private lateinit var binding: FragmentPhotosBinding
    private val args: PhotosFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_photos, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.pagerImages.adapter = PhotosAdapter(context!!, args.photos.toList())
        binding.pagerImages.currentItem = args.position
        binding.imageBack.setOnClickListener { findNavController().popBackStack() }
    }
}