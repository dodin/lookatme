package com.dodin.look.ui.subscribers

import com.dodin.look.common.CoroutineViewModel
import com.dodin.look.common.Lce
import com.dodin.look.common.ViewStateStore
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SubscribersViewModel(val repo: Repository) : CoroutineViewModel() {
    val subscribers = ViewStateStore<Lce<List<User>>>(Lce.Loading)
    val subscriptions = ViewStateStore<Lce<List<User>>>(Lce.Loading)

    init {
        getSubscribers()
        getSubscriptions()
    }

    fun getSubscribers() = launch(Dispatchers.Main) {
        val response = repo.getSubscribers()
        subscribers.dispatchState(response)
    }

    fun getSubscriptions() = launch(Dispatchers.Main) {
        val response = repo.getSubscriptions()
        subscriptions.dispatchState(response)
    }

    /**
     * Subscribe or unsubscribe to user
     * @param uid User's id
     */
    fun subscribe(uid: Int) = launch(Dispatchers.Main) {
        val response = repo.subscribe(uid)
        if (response !is Lce.Success) {
            return@launch
        }

        // Update subscribers list
        if (subscribers.state() is Lce.Success) {
            val list = subscribers.state().data!!.map {
                // Switch subscription state
                if (it.id == response.data.subscriberId) {
                    return@map it.copy(subscribed = response.data.subscribed)
                } else {
                    return@map it
                }
            }
            subscribers.dispatchState(Lce.Success(list))
        }

        // Update subscriptions list
        if (subscriptions.state() is Lce.Success) {
            // Resulting list will contain only users to which we subscribed
            val list = subscribers.state().data!!.filter {
                response.data.subscribed && it.id == response.data.subscriberId
            }
            subscriptions.dispatchState(Lce.Success(list))
        }
    }
}