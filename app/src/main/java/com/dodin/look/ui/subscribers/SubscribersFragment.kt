package com.dodin.look.ui.subscribers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.dodin.look.R
import com.dodin.look.common.Lce
import com.dodin.look.data.dto.User
import com.dodin.look.ui.post.PostFragmentDirections
import com.google.android.material.tabs.TabLayout
import org.koin.androidx.viewmodel.ext.android.viewModel


class SubscribersFragment : Fragment() {
    private lateinit var binding: com.dodin.look.databinding.FragmentSubscribersBinding
    private val vm: SubscribersViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_subscribers, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())

        binding.tabs.addTab(binding.tabs.newTab().apply { setText(R.string.profile_subscribers_label) })
        binding.tabs.addTab(binding.tabs.newTab().apply { setText(R.string.profile_subscriptions_label) })
        binding.tabs.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(p0: TabLayout.Tab) {
                if (p0.position == 0) {
                    val subscribers = vm.subscribers.state().data ?: listOf()
                    showSubscribers(subscribers)
                } else {
                    val subscriptions = vm.subscriptions.state().data ?: listOf()
                    showSubscriptions(subscriptions)
                }
            }

            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }
        })

        vm.subscribers.observe(viewLifecycleOwner) {
            when(it) {
                is Lce.Loading -> binding.stateLayout.loading()
                is Lce.Success -> {
                    binding.stateLayout.content()
                    showSubscribers(it.data)
                }
                is Lce.Error -> binding.stateLayout.info().infoTitle(it.message)
            }
        }
    }

    private fun showSubscribers(data: List<User>) {
        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.setHasFixedSize(true)
        binding.list.adapter = SubscribersAdapter().apply {
            submitList(data)

            clickListener = object : SubscriberClickListener {
                override fun onSubscribe(user: User) {
                    vm.subscribe(user.id)
                }

                override fun onClick(user: User) {
                    navigateToUser(user)
                }
            }
        }
    }

    private fun showSubscriptions(data: List<User>) {
        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.setHasFixedSize(true)
        binding.list.adapter = SubscribersAdapter().apply {
            submitList(data)

            clickListener = object : SubscriberClickListener {
                override fun onSubscribe(user: User) {
                    vm.subscribe(user.id)
                }

                override fun onClick(user: User) {
                    navigateToUser(user)
                }
            }
        }
    }

    private fun navigateToUser(user: User) {
        val directions = PostFragmentDirections.showProfile(user)
        findNavController().navigate(directions)
    }
}