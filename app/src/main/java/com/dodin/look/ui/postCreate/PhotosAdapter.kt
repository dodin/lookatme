package com.dodin.look.ui.postCreate

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.squareup.picasso.Picasso


class PhotosAdapter(private val listener: PhotoListener? = null) :
    ListAdapter<Uri, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            (holder as PhotoViewHolder).bind(getItem(position), listener)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Uri>() {
            override fun areItemsTheSame(oldItem: Uri, newItem: Uri): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Uri, newItem: Uri): Boolean {
                return oldItem.path == newItem.path
            }
        }
    }
}

class PhotoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private val photo: ImageView = view.findViewById(R.id.image)
    private val delete: ImageView = view.findViewById(R.id.imageDelete)

    fun bind(uri: Uri, listener: PhotoListener?) {
        photo.scaleType = ImageView.ScaleType.FIT_CENTER
        photo.layoutParams.height = view.context.resources.getDimension(R.dimen.add_photo_height).toInt()
        Picasso.get()
            .load(uri)
            .resize(0, 200)
            .into(photo)

        photo.setOnClickListener {
            listener?.itemClicked(adapterPosition)
        }

        delete.setOnClickListener {
            listener?.removeClicked(adapterPosition)
        }
    }
}

interface PhotoListener {
    fun removeClicked(position: Int)
    fun itemClicked(position: Int)
}