package com.dodin.look.ui.postCreate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.dodin.look.data.dto.Item


class ItemsAdapter(private val listener: PhotoListener? = null) :
    ListAdapter<Item, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(getItem(position), listener)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.title == newItem.title && oldItem.color == newItem.color && newItem.size == newItem.size
            }
        }
    }
}

class ItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private val textTitle: TextView = view.findViewById(R.id.textItemName)
    private val textColor: TextView = view.findViewById(R.id.textItemColor)
    private val textSize: TextView = view.findViewById(R.id.textItemSize)
    private val imageDelete: ImageView = view.findViewById(R.id.imageDelete)

    fun bind(item: Item, listener: PhotoListener?) {
        textTitle.text = item.title
        textColor.text = item.color
        textSize.text = item.size

        view.setOnClickListener {
            listener?.itemClicked(adapterPosition)
        }

        imageDelete.setOnClickListener {
            listener?.removeClicked(adapterPosition)
        }
    }
}