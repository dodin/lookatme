package com.dodin.look.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.dodin.look.data.local.dto.Post
import com.squareup.picasso.Picasso

class PostsAdapter : PagedListAdapter<Post, PostViewHolder>(DIFF_CALLBACK) {
    interface ActionListener {
        fun onItemClick(post: Post, view: View)
    }

    var listener: ActionListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bindTo(getItem(position)!!, listener)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.title == newItem.title
            }
        }
    }
}

class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val textTitle: TextView = view.findViewById(R.id.textTitle)
    private val imagePreview: ImageView = view.findViewById(R.id.imagePreview)
    private val textLikes: TextView = view.findViewById(R.id.textLikes)
    private val imageLike: ImageView = view.findViewById(R.id.imageLike)

    fun bindTo(item: Post, listener: PostsAdapter.ActionListener?) {
        textTitle.text = item.title
        textLikes.text = item.likes.toString()
        if (item.photos.isNotEmpty()) {
            Picasso.get()
                .load(item.photos[0])
                .into(imagePreview)
        }
        imageLike.setImageResource(if (item.like) R.drawable.ic_favorite else R.drawable.ic_favorite_border)

        itemView.setOnClickListener { listener?.onItemClick(item, itemView) }
    }
}