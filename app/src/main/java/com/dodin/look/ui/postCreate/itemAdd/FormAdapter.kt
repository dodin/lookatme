package com.dodin.look.ui.postCreate.itemAdd

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.dodin.look.R
import com.dodin.look.data.dto.Item

class FormAdapter(val elements: MutableList<Item>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            (holder as FormViewHolder).bind(elements[position], elements)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_NORMAL) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_item_edit, parent, false)
            FormViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add, parent, false)
            FooterViewHolder(view, this)
        }
    }

    override fun getItemCount(): Int = elements.size + 1

    override fun getItemViewType(position: Int): Int {
        return if (position == elements.size) TYPE_FOOTER else TYPE_NORMAL
    }

    fun add() {
        elements.add(Item())
        notifyItemInserted(elements.size - 1)
    }

    companion object {
        const val TYPE_NORMAL = 1
        const val TYPE_FOOTER = 2
    }
}

class FormViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val name: EditText = view.findViewById(R.id.editItemName)
    private val size: EditText = view.findViewById(R.id.editItemSize)
    private val color: EditText = view.findViewById(R.id.editItemColor)

    fun bind(item: Item, elements: MutableList<Item>) {
        name.setText(item.title)
        size.setText(item.size)
        color.setText(item.color)
        name.requestFocus()

        name.addTextChangedListener(onTextChanged = { text, _, _, _ ->
            elements[adapterPosition] = elements[adapterPosition].copy(title = text.toString())
        })

        size.addTextChangedListener(onTextChanged = { text, _, _, _ ->
            elements[adapterPosition] = elements[adapterPosition].copy(size = text.toString())
        })

        color.addTextChangedListener(onTextChanged = { text, _, _, _ ->
            elements[adapterPosition] = elements[adapterPosition].copy(color = text.toString())
        })
    }
}

class FooterViewHolder(view: View, adapter: FormAdapter) : RecyclerView.ViewHolder(view) {
    init {
        view.setOnClickListener {
            adapter.add()
        }
    }
}