package com.dodin.look.ui.post

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.dodin.look.R
import com.dodin.look.common.*
import com.dodin.look.data.dto.Comment
import com.dodin.look.data.dto.Post
import com.dodin.look.databinding.FragmentPostBinding
import com.dodin.look.ui.auth.AuthActivity
import com.google.android.material.appbar.AppBarLayout
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.math.abs

class PostFragment : Fragment(), ImagesAdapter.ImageClickListener, AppBarLayout.OnOffsetChangedListener {
    private lateinit var binding: FragmentPostBinding
    private val vm: PostViewModel by viewModel()
    private val appUtils: AppUtils by inject()

    private var mIsImageHidden = false
    private var mMaxScrollSize = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        NavigationUI.setupWithNavController(binding.collapsingToolbar, binding.toolbar, findNavController())
        binding.appbar.addOnOffsetChangedListener(this)
        binding.imageLike.setOnClickListener { like() }
        binding.imageSend.setOnClickListener { submitComment() }

        binding.editComment.setFetcher {
            vm.fetchMentions(it)
        }

        // TODO: refactor to use only id
        val postId = arguments?.getInt("post_id")
        if (postId != null) {
            vm.loadPost(postId)
        } else {
            val post = arguments?.getParcelable<Post>("post")
            if (post != null) {
                vm.post.dispatchState(Lce.Success(post))
                vm.fetchComments(post.id)
            }
        }

        vm.comments.observe(viewLifecycleOwner) {
            if (it is Lce.Success) {
                bindComments(it.data)
                binding.editComment.setText("")
            }
        }

        vm.post.observe(viewLifecycleOwner) {
            if (it is Lce.Success) {
                bindPost(it.data)
            }
        }

        vm.mentions.observe(viewLifecycleOwner) {
            if (it is Lce.Success) {
                binding.editComment.onReceiveSuggestionsResult(it.data)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AuthActivity.RC_AUTH && resultCode == Activity.RESULT_OK) {
            val referrer = data?.getStringExtra(AuthActivity.REFERRER)
            when (referrer) {
                LIKE -> like()
                COMMENT -> submitComment()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onImageClicked(images: List<String>, position: Int) {
        val directions = PostFragmentDirections.showPhotos(position, images.toTypedArray())
        findNavController().navigate(directions)
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = binding.appbar.totalScrollRange

        val currentScrollPercentage = abs(verticalOffset) * 100 / mMaxScrollSize

        // open
        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true

                ViewCompat.animate(binding.layoutUser).scaleY(0f).scaleX(0f).start()
            }
        }

        // closed
        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false
                ViewCompat.animate(binding.layoutUser).scaleY(1f).scaleX(1f).start()
            }
        }
    }

    private fun bindPost(post: Post) {
        if (vm.isCurrentUser) {
            binding.toolbar.menu.clear()
            binding.toolbar.inflateMenu(R.menu.post_author)
            binding.toolbar.setOnMenuItemClickListener {
                if (it.itemId == R.id.menu_edit) {
                    val args = Bundle().apply {
                        putParcelable("post", post)
                        putInt("post_id", post.id)
                    }
                    findNavController().navigate(R.id.postCreateFragment, args)
                }
                true
            }
        }

        binding.appbar.showTitleCollapsed(binding.collapsingToolbar, post.title)
        binding.imageLike.setImageResource(if (post.like) R.drawable.ic_favorite else R.drawable.ic_favorite_border)
        binding.imageLike.tint = R.color.like

        binding.textDescription.text = post.description
        binding.textDescription.movementMethod = LinkMovementMethod.getInstance()
        binding.textDescription.autoLink(listOf(PATTERN_HASHTAG)) {
            val directions = PostFragmentDirections.searchTag(it)
            findNavController().navigate(directions)
        }

        // Photos
        if (binding.pagerImages.adapter == null) {
            binding.pagerImages.adapter = ImagesAdapter(context!!, post.photos, this)
        } else {
            (binding.pagerImages.adapter as ImagesAdapter).submitList(post.photos)
        }

        // Items
        with(binding.listItems) {
            layoutManager = LinearLayoutManager(context)
            adapter = ItemsAdapter().apply {
                submitList(post.items)
            }
            setHasFixedSize(true)
        }

        // User
        binding.textUserName.text = post.author.name
        Picasso.get().load(post.author.photo).transform(CircleTransform()).into(binding.imageUserPhoto)
//        binding.appbar.addOnOffsetChangedListener(this)
        binding.imageUserPhoto.setOnClickListener {
            val directions = PostFragmentDirections.showProfile(post.author)
            findNavController().navigate(directions)
        }
    }

    private fun bindComments(comments: List<Comment>) {
        with(binding.listComments) {
            layoutManager = LinearLayoutManager(context)
            adapter = CommentsAdapter().apply {
                submitList(comments)
            }
            setHasFixedSize(true)
        }
    }

    private fun like() {
        if (appUtils.isAuthenticated()) {
            vm.like()
        } else {
            startActivityForResult(AuthActivity.intent(context!!, LIKE), AuthActivity.RC_AUTH)
        }
    }

    private fun submitComment() {
        if (appUtils.isAuthenticated()) {
            vm.addComment(binding.editComment.text.toString())
        } else {
            startActivityForResult(AuthActivity.intent(context!!, COMMENT), AuthActivity.RC_AUTH)
        }
    }

    companion object {
        const val PERCENTAGE_TO_SHOW_IMAGE = 80
        const val LIKE = "like"
        const val COMMENT = "comment"
    }
}