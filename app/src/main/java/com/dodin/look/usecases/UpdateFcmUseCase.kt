package com.dodin.look.usecases

import com.dodin.look.data.Repository
import com.dodin.look.data.local.Preferences
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UpdateFcmUseCase(private val repo: Repository, private val pref: Preferences) {
    fun run() {
        if (pref.fcmToken.isEmpty() && pref.token != null) {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token
                    if (token != null) {
                        pref.fcmToken = token

                        GlobalScope.launch {
                            repo.updateFcmToken(token)
                        }
                    }
                })
        }
    }
}