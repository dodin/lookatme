package com.dodin.look.common.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Type converter for Room.
 * Converts list of strings to json array string and backwards.
 */
class StringListTypeConverter {
    private val gson = Gson()

    @TypeConverter
    fun stringToList(data: String): List<String> {
        val listType = object : TypeToken<List<String>>() { }.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun listToString(data: List<String>): String {
        return gson.toJson(data)
    }
}