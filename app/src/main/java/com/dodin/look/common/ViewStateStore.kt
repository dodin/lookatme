package com.dodin.look.common

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async

class ViewStateStore<T : Any>(
        initialState: T
) {
    private val liveData = MutableLiveData<T>().apply {
        value = initialState
    }

    public fun getLiveData(): LiveData<T> {
        return liveData
    }

    fun observe(owner: LifecycleOwner, observer: (T) -> Unit) =
            liveData.observe(owner, Observer { observer(it!!) })

    fun dispatchState(state: T) {
        liveData.value = state
    }

    fun postState(state: T) {
        liveData.postValue(state)
    }

    fun state() = liveData.value!!
}

inline fun <S> CoroutineScope.lce(store: ViewStateStore<Lce<S>>, crossinline f: suspend () -> S) {
    store.dispatchState(Lce.Loading)
    try {
        async {
            val data = f()
            store.dispatchState(Lce.Success(data))
        }
    } catch (e: Exception) {
        store.dispatchState(Lce.Error(e.message ?: "Error"))
    }

}