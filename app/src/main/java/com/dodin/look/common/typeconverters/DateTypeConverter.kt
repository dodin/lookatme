package com.dodin.look.common.typeconverters

import androidx.room.TypeConverter
import java.util.*

/**
 * Type converter for room.
 * Converts Date to Long and Long to Date.
 */
class DateTypeConverter {
    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun timestampToDate(timestamp: Long?): Date? {
        return timestamp?.let { Date(it) }
    }
}