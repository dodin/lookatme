package com.dodin.look.common

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("android:visibility")
fun isVisible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter(value = ["imageUrl", "round"], requireAll = false)
fun loadImage(view: ImageView, imageUrl: String?, round: Boolean = false) {
    if (imageUrl == null || imageUrl.isEmpty()) {
        return
    }

    val reqCreator = Picasso.get()
        .load(imageUrl)

    if (round) {
        reqCreator.transform(CircleTransform())
    }
    reqCreator.into(view)
}