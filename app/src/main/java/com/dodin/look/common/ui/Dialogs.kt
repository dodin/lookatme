package com.dodin.look.common.ui

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import com.dodin.look.R

/**
 * Show custom progress dialog
 */
fun createProgressDialog(ctx: Context): Dialog {
    val view = LayoutInflater.from(ctx).inflate(R.layout.card_progress, null, false)

    val progressDialog = Dialog(ctx)
    progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    progressDialog.setContentView(view)
    progressDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
    progressDialog.setCancelable(false)

    return progressDialog
}