package com.dodin.look.common

import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat

var Drawable.tint: Int
    @ColorInt get() = tint
    set(@ColorInt value) {
        if (Build.VERSION.SDK_INT >= 21) setTint(value)
        else DrawableCompat.setTint(DrawableCompat.wrap(this), value)
    }

var ImageView.tint: Int
    @ColorInt get() = solidColor
    set(@ColorRes value) {
        setColorFilter(ContextCompat.getColor(context, value), android.graphics.PorterDuff.Mode.SRC_IN)
    }