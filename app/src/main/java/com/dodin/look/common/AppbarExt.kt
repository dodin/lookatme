package com.dodin.look.common

import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout



/**
 * Show title only when collapsing toolbar layout is minimized
 */
fun AppBarLayout.showTitleCollapsed(collapsingToolbar: CollapsingToolbarLayout, title: String) {
    addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
        val collapsed = Math.abs(verticalOffset) >= appBarLayout.totalScrollRange

        if (!collapsed) {
            collapsingToolbar.title = " " // Careful! There should be a space between double quote. Otherwise it won't work.
        } else {
            collapsingToolbar.title = title
        }
    })
}