package com.dodin.look.common

import android.content.ContentResolver
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import androidx.fragment.app.Fragment
import com.dodin.look.data.dto.Comment
import com.dodin.look.data.dto.User
import com.dodin.look.data.local.Preferences
import com.dodin.look.ui.auth.AuthActivity
import org.json.JSONObject
import java.nio.charset.Charset

class AppUtils(private val pref: Preferences) {
    fun getCurrentUserId(): Int {
        if (getCurrentUserIdFromCache() != null) {
            return getCurrentUserIdFromCache()!!
        }

        val jwt = pref.token?.accessToken ?: return  0

        val bodyBase64 = jwt.substring(jwt.indexOf(".") + 1, jwt.lastIndexOf("."))
        val body = String(Base64.decode(bodyBase64, Base64.NO_WRAP), Charset.forName("utf-8"))
        val json = JSONObject(body)
        val userId = json.getInt("sub")
        return userId
    }

    private fun getCurrentUserIdFromCache(): Int? {
        return  pref.user?.id
    }

    fun isCurrentUser(user: User): Boolean {
        return getCurrentUserId() == user.id
    }

    fun isAuthenticated(): Boolean {
        return pref.token != null
    }
}

/**
 * Function search for tempComments in comments.
 * In case temp comment found it will be removed from both lists.
 */
fun replaceTempComment(comments: List<Comment>, tempComments: MutableList<Comment>): List<Comment> {
    return comments.filter { comment ->
        tempComments.forEachIndexed { index, temp ->
            if (temp.id == comment.id && temp.content == comment.content) {
                tempComments.removeAt(index)
                return@filter false
            }
        }

        true
    }
}

/**
 * Execute block of code only if user authenticated.
 * Run auth activity otherwise.
 */
fun Fragment.ifAuthenticated(appUtils: AppUtils, referrer: String, block: () -> Unit) {
    if (appUtils.isAuthenticated()) {
        block()
    } else {
        startActivityForResult(AuthActivity.intent(context!!, referrer), AuthActivity.RC_AUTH)
    }
}

/**
 * Query file path by it's uri
 */
fun ContentResolver.queryPath(contentUri: Uri): String {
    // can post image
    val projection = arrayOf(MediaStore.Images.Media.DATA)
    val cursor = query(
        contentUri,
        projection, // WHERE clause selection arguments (none)
        null, null, null
    )

    cursor?.use {
        // Which columns to return
        // WHERE clause; which rows to return (all rows)
        // Order-by clause (ascending by name)
        val columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val path = cursor.getString(columnIndex)
        cursor.close()
        return path
    }

    // If cursor is null then return empty path
    return ""
}