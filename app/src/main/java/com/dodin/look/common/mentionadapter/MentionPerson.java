package com.dodin.look.common.mentionadapter;

import com.google.gson.annotations.SerializedName;

public class MentionPerson {

    @SerializedName("value")
    public String name;
    @SerializedName("uid")
    public String id;
    @SerializedName("image")
    public String imageURL;
    @SerializedName("username")
    public String username;

    public String getFormattedValue() {
        return "@[" + username + "](" + id + ")";
    }

    public MentionPerson() {
    }

    public MentionPerson(String username, String name, String id, String imageURL) {
        this.username = username;
        this.name = name;
        this.id = id;
        this.imageURL = imageURL;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getImageURL() {
        return imageURL;
    }

    @Override
    public String toString() {
        return username;
    }
}