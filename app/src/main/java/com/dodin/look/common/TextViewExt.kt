package com.dodin.look.common

import android.graphics.Paint
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.dodin.look.R
import java.util.regex.Pattern

fun TextView.linkify() {
    setTextColor(ContextCompat.getColor(context, R.color.link))
    paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
}

fun TextView.autoLink(patterns: List<Pattern>, listener: ((link: String) -> Unit)? = null) {
    val spannableString = SpannableString(text)

    patterns.forEach {
        val matcher = it.matcher(text)
        while (matcher.find()) {
            val element = matcher.group(0)
            val indexStart = text.indexOf(element)
            val indexEnd = indexStart + element.length

            val spanClickListener: ClickableSpan = object : ClickableSpan() {
                override fun onClick(p0: View) {
                    listener?.invoke(element)
                }
            }
            spannableString.setSpan(spanClickListener, indexStart, indexEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

    text = spannableString
}

val PATTERN_HASHTAG = Pattern.compile("#\\s*(\\w+)")
val PATTERN_MENTION = Pattern.compile("@\\s*(\\w+)")