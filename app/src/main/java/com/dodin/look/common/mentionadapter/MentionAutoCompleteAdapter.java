package com.dodin.look.common.mentionadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.dodin.look.R;
import com.dodin.look.common.CircleTransform;
import com.squareup.picasso.Picasso;


import java.util.List;

public class MentionAutoCompleteAdapter extends ArrayAdapter<MentionPerson> {

    public MentionAutoCompleteAdapter(@NonNull Context context, @NonNull List<MentionPerson> objects) {
        super(context, 0, objects);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Nullable
    @Override
    public MentionPerson getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(@Nullable MentionPerson item) {
        return super.getPosition(item);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;

        ViewHolder viewHolder;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mention_people, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.textView.setText(getItem(position).getName());
        viewHolder.textView2.setText(getItem(position).getUsername());
        Picasso.get().load(getItem(position).imageURL).transform(new CircleTransform()).into(viewHolder.imagePhoto);

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        ViewHolder viewHolder;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mention_people, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.textView.setText(getItem(position).getName());

        return view;
    }

    static class ViewHolder {
        ImageView imagePhoto;
        TextView textView;
        TextView textView2;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            textView = rootView.findViewById(R.id.textView);
            textView2 = rootView.findViewById(R.id.textView2);
            imagePhoto = rootView.findViewById(R.id.imagePhoto);
        }
    }
}