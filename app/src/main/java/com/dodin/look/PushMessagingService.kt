package com.dodin.look

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import com.dodin.look.data.Repository
import com.dodin.look.data.dto.User
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

class PushMessagingService : FirebaseMessagingService() {

    val repository: Repository by inject()
    val gson: Gson by inject()

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.data?.isNotEmpty()?.let {
            Timber.d("Message data payload: %s", remoteMessage.data)
            val type = remoteMessage.data["type"]
            when(type) {
                "mention" -> {
                    val userJson: String = remoteMessage.data["mentioned_by"] as String
                    val user: User = gson.fromJson(userJson, User::class.java)
                    val postId = remoteMessage.data["post_id"]!!.toInt()

                    val args = Bundle().apply {
                        putInt("post_id", postId)
                    }

                    val pendingIndent = NavDeepLinkBuilder(applicationContext)
                        .setGraph(R.navigation.nav_graph)
                        .setDestination(R.id.postFragment)
                        .setArguments(args)
                        .createPendingIntent()

                    val title = getString(R.string.app_name)
                    val content  = getString(R.string.notification_mention, user.name)

                    createNotificationChannel()
                    showNotification(title, content, pendingIndent)
                }
            }
        }

        // Check if message contains a notification payload.
        remoteMessage?.notification?.let {
            Timber.d("Message Notification Body: ${it.body}")
        }

    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        GlobalScope.launch {
            repository.updateFcmToken(token)
        }
    }

    private fun showNotification(title: String, content: String, contentIntent: PendingIntent) {
        var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle(title)
            .setContentText(content)
            .setAutoCancel(true)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setContentIntent(contentIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, builder.build())
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notification_channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }


    companion object {
        const val CHANNEL_ID = "LookAtMe Push Notifications"
    }
}