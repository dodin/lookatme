package com.dodin.look.di

import androidx.room.Room
import com.dodin.look.common.AppUtils
import com.dodin.look.common.typeconverters.TimestampTypeAdapter
import com.dodin.look.data.Repository
import com.dodin.look.data.local.Preferences
import com.dodin.look.data.local.db.AppDatabase
import com.dodin.look.data.mapper.PostMapper
import com.dodin.look.data.remote.Api
import com.dodin.look.data.remote.AuthService
import com.dodin.look.data.remote.BackendService
import com.dodin.look.data.remote.Oauth
import com.dodin.look.ui.auth.AuthViewModel
import com.dodin.look.ui.main.MainViewModel
import com.dodin.look.ui.post.PostViewModel
import com.dodin.look.ui.postCreate.PostCreateViewModel
import com.dodin.look.ui.profile.ProfileViewModel
import com.dodin.look.ui.profileEdit.ProfileEditViewModel
import com.dodin.look.ui.subscribers.SubscribersViewModel
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.util.*

val dataModule = module {
    single { GsonBuilder().registerTypeAdapter(Date::class.java, TimestampTypeAdapter()).create() }
    single { Preferences(androidContext(), get()) }
    single { Room.databaseBuilder(androidContext(), AppDatabase::class.java, "look_cache.db").build() }
    single { PostMapper(get()) }
    single { AppUtils(get()) }
    single { AuthService(get(), get()) as Oauth }
    single { BackendService(get(), get()) as Api }
    single { Repository(get(), get(), get(), get(), get()) }
}

val viewModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { PostViewModel(get(), get()) }
    viewModel { ProfileViewModel(get(), get(), get()) }
    viewModel { ProfileEditViewModel(get()) }
    viewModel { PostCreateViewModel(get()) }
    viewModel { AuthViewModel(get(), get()) }
    viewModel { SubscribersViewModel(get()) }
}

val appModules = listOf(dataModule, viewModule)